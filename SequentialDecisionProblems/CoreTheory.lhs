> module SequentialDecisionProblems.CoreTheory where

> import Data.List

---------Begin of "interface"------------------------------------

> data State

> data Ctrl
  
> type Nat = Int

> data Val

> zero :: Val

> data M a

> nexts         ::  State -> Ctrl -> M State
> reward        ::  State -> Ctrl -> State -> Val
> meas          ::  M Val -> Val

> type Policy   =  State -> Ctrl
> -- Policy t (S m)  =  (x : State t) -> Reachable x -> Viable (S m) x -> GoodCtrl t x m

> type PolicySeq = [Policy]

> cval     ::  State -> PolicySeq -> Ctrl -> Val

> val      :: State -> PolicySeq -> Val

> admissible  ::  State -> Ctrl -> Bool

> optExt   ::  PolicySeq -> Policy

-- val (optExt ps s : ps) s >= val (p : ps) s for "all" s, p, ps

> backwardsInduction  ::  Nat -> PolicySeq

> type StateCtrlSeq    =  [(State, Maybe Ctrl)]

> trj                 ::  PolicySeq -> State -> M StateCtrlSeq

--------Important, but not used----------------------------------

> melem        ::  a -> M a -> Bool
> mnull        ::  M a -> Bool
> mall         ::  (a -> Bool) -> M a -> Bool
> mtag         ::  M a -> M (a, Bool)
> viable       ::  State -> Nat -> Bool

> good         ::  State -> Ctrl -> Nat -> Bool

> reachable    :: State -> Bool

-------Default implementation------------------------------------

> good s c n    =  not (mnull ms) && mall (\ a -> viable a n) ms
>                  where
>                  ms = nexts s c

> cval s ps c  =  meas mr
>   where
>   ms = nexts s c
>   mr = fmap (\ s' -> reward s c s' + val s' ps) ms

> val _ []  =  zero
> val s (p : ps) = cval s ps (p s)

> optExt  ps  s   =  snd (head (sortBy o [(cval s ps c, c) | c <- [minBound .. maxBound], admissible s c]))
>   where
>     o (v1, c1) (v2, c2)   =  if v1 > v2
>                               then LT
>                               else if v1 < v2 then GT
>                                               else EQ


> backwardsInduction n  =  if n == 0
>                             then []
>                             else optExt ps : ps
>                          where ps = backwardsInduction (n - 1)

> trj []       s       =  return [(s, Nothing)]
> trj (p : ps) s       =  fmap ((s, Just (p s)) : ) (nexts s (p s) >>= trj ps)

-----------User-defined implementations-------------------------

> instance Eq Val where
>   (==)  =  undefined
> instance Ord Val where
>   (<=)  =  undefined

> zero  = undefined

> instance Num Val where
>   (+)     =  undefined
>   (*)     =  undefined
>   abs     =  undefined
>   signum  =  undefined
>   fromInteger  =  undefined

> instance Functor M where
>   fmap     = undefined

> instance Monad M where
>   return   =  undefined
>   (>>=)    =  undefined

> instance Bounded Ctrl where
>   minBound =  undefined
>   maxBound =  undefined
> instance Enum Ctrl   where
>   fromEnum = undefined
>   toEnum   = undefined

> nexts      =  undefined
> reward     =  undefined
> meas       =  undefined
> admissible =  undefined

  > optExt ps s  =  undefined

---------User-defined if needed----------------------------------

> melem      =  undefined
> mnull      =  undefined
> mall       =  undefined
> mtag       =  undefined
> viable     =  undefined
> reachable  =  undefined



