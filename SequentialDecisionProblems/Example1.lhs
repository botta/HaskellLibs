> {-# LANGUAGE GeneralizedNewtypeDeriving #-}
> module SequentialDecisionProblems.Example1 where

> import Data.List
> import Control.Monad.Identity

> maxColumn  :: Nat
> maxColumn  =  10

> nColumns  ::  Nat
> nColumns   =  maxColumn + 1


---------Begin of "interface"------------------------------------



> newtype State = State Nat --  State t = LTB nColumns
>                 deriving (Show, Eq, Ord, Enum)

> instance Bounded State where
>   maxBound = State maxColumn
>   minBound = State 0


> data Ctrl  =  L | A | R
>                 deriving (Show, Eq, Enum, Bounded)
  
> type Nat = Int

> type Val = Nat

> zero  ::  Val

> type M a       =  Identity a

> nexts               ::  State -> Ctrl -> M State
  
> reward                        ::  State -> Ctrl -> State -> Val
> reward (State s) c (State s')
>   | s' == 0                   =  1
>   | s' ==  maxColumn          =  2
>   | s' > 0 && s' < maxColumn  =  0
  
> meas              ::  M Val -> Val
> meas (Identity n)  =  n

> type Policy   =  State -> Ctrl
> -- Policy t (S m)  =  (x : State t) -> Reachable x -> Viable (S m) x -> GoodCtrl t x m

> type PolicySeq = [Policy]

> cval     ::  State -> PolicySeq -> Ctrl -> Val

> val      ::  State -> PolicySeq -> Val

> optExt   ::  PolicySeq -> Policy

-- val (optExt ps s : ps) s >= val (p : ps) s for "all" s, p, ps

> backwardsInduction  ::  Nat -> PolicySeq

--------Important, but not used----------------------------------

> melem        ::  a -> M a -> Bool
> mnull        ::  M a -> Bool
> mall         ::  (a -> Bool) -> M a -> Bool
> mtag         ::  M a -> M (a, Bool)
> viable       ::  State -> Nat -> Bool

> good         ::  State -> Ctrl -> Nat -> Bool

> reachable    ::  State -> Bool

-------Default implementation------------------------------------

> good s c n    =  not (mnull ms) && mall (\ a -> viable a n) ms
>                  where
>                  ms = nexts s c

> cval s ps c   =  meas mr
>   where
>   ms  =  nexts s c
>   mr  =  fmap (\ s' -> reward s c s' + val s' ps) ms

> val _ []        =  zero
> val s (p : ps)  =  cval s ps (p s)

> backwardsInduction n  =  if n == 0
>                             then []
>                             else optExt ps : ps
>                          where ps = backwardsInduction (n - 1)

> trj []       s       =  return [(s, Nothing)]
> trj (p : ps) s       =  fmap ((s, Just (p s)) : ) (nexts s (p s) >>= trj ps)

-----------User-defined implementations-------------------------

> zero   =  0

> nexts (State n) A    =  Identity (State n)
> nexts (State n) L    =  if n == 0
>                            then Identity (State maxColumn)
>                            else Identity (State (n - 1))
> nexts (State n) R    =  if n == maxColumn
>                            then Identity (State 0)
>                            else Identity (State (n + 1))


> optExt  ps  s   =  snd (head (sortBy o [(cval s ps c, c) | c <- [minBound .. maxBound]]))
>   where
>     o (v1, c1) (v2, c2)   =  if v1 > v2
>                               then LT
>                               else if v1 < v2 then GT
>                                               else EQ

---------User-defined if needed----------------------------------

> melem      =  undefined
> mnull      =  undefined
> mall       =  undefined
> mtag       =  undefined
> viable     =  undefined
> reachable  =  undefined






> {-



* The computation:

> computation : { [STDIO] } Eff ()
> computation =
>   do putStr ("enter number of steps:\n")
>      nSteps <- getNat
>      putStr ("enter initial column:\n")
>      x0 <- getLTB nColumns
>      case (dViable {t = Z} nSteps x0) of
>        (Yes v0) => do putStrLn ("computing optimal policies ...")
>                       ps   <- pure (backwardsInduction Z nSteps)
>                       putStrLn ("computing optimal controls ...")
>                       mxys <- pure (possibleStateCtrlSeqs x0 () v0 ps)
>                       putStrLn (show mxys)
>                       putStrLn ("done!")                       
>        (No _)   => putStrLn ("initial column non viable for " ++ cast {from = Int} (cast nSteps) ++ " steps")

> main : IO ()
> main = run computation


<

> ---}
