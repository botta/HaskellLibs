> module Iter.SimpleProb.MicroTrj(MicroTrj, 
>                                 makeMicroTrj, 
>                                 isEnd, 
>                                 eval, 
>                                 next, 
>                                 visit) where


> import List
> import Maybe

> import NumericTypes.Nat
> import Prob.SimpleProb
> import Prob.SimpleProbOps


The MicroTrj iterator type

> data MicroTrj a = MicroTrj (a -> SimpleProb a) [[(a,Prob)]]


Helpers

> theSys :: MicroTrj a -> (a -> SimpleProb a)
> theSys (MicroTrj f apss) = f

> theData :: MicroTrj a -> [[(a,Prob)]]
> theData (MicroTrj f apss) = apss

> grow :: (a -> SimpleProb a) -> [[(a,Prob)]] -> Nat -> [[(a,Prob)]]
> grow f apss 0 = apss
> grow f apss (k + 1) = (aps:apss')
>     where aps = unwrapSP spa
>           spa = f b
>           (b,p) = head (head apss')
>           apss' = grow f apss k


Constructors

> makeMicroTrj :: (a -> SimpleProb a) -> a -> Nat -> MicroTrj a
> makeMicroTrj f a 0 = MicroTrj f [[(a,1.0)]]
> makeMicroTrj f a (k + 1) = MicroTrj f (aps:apss)
>     where aps = unwrapSP spa
>           spa = f b
>           (b,p) = head (head apss)
>           apss = theData (makeMicroTrj f a k)


Iterator interface

> isEnd :: MicroTrj a -> Bool
> isEnd (MicroTrj f apss) = isNothing(findIndex (( > 1).length) apss)

> eval :: MicroTrj a -> ([a],Prob)
> eval (MicroTrj f apss) = (as,p)
>     where (as,ps) = unzip (map head apss)
>           p = product ps

> next :: MicroTrj a -> MicroTrj a
> next (MicroTrj f apss) = MicroTrj f apss'
>     where apss' = grow f apss'' i
>           apss'' = (aps:apss''')
>           (ap:aps) = apss!!i
>           apss''' = [apss!!j | j <- [(i + 1)..(n - 1)]]
>           n = length apss
>           i = fromJust (findIndex (( > 1).length) apss)

This would probably have to go into an Iter type class

> visit ::  (Show a) => 
>           MicroTrj a -> 
>           IO ()
> visit mt = do done <- return (isEnd mt)
>               if (done == True) 
>               then print (eval mt)
>               else (do print (eval mt)
>                        visit (next mt))

