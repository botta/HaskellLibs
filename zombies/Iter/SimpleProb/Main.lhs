#!/usr/bin/runhugs -98

-- set -98 option in HUGSFLAGS !

--#!/usr/bin/runhugs -98


> module Main where

> import NumericTypes.Nat
> -- import NumericTypes.Real
> -- import Util.List.Ops
> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Iter.SimpleProb.MicroTrj

Convenience types

Global parameters:

transition function, initial value, number of interations ...

> st :: Integer -> SimpleProb Integer
> st n = SP [(n - 1, 0.4), (n + 1, 0.6)]

> x :: Integer
> x = 0

> n :: Nat
> n = 2


Construct MicroTrj iterator

> mt :: MicroTrj Integer
> mt = makeMicroTrj st x n


Visit the trajectories

> main = visit mt


