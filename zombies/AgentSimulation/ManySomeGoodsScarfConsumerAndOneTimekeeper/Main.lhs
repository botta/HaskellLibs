#!/usr/bin/runhugs -98

-- set -98 option in HUGSFLAGS !

--#!/usr/bin/runhugs -98


> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import NumericTypes.Real
> import Util.List.Ops
> import Util.Unsafe.Random.Ops
> import Prob.SimpleProb
> import Prob.SimpleProbOps

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule
> import Timekeepers.Timekeeper

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsId = [AgentId]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters:

number of goods, agents, iterations ...

> nGoods :: Nat
> nGoods = 3

> nInitialPrices :: Nat
> nInitialPrices = 11

> nAgentsPerGood :: Nat
> nAgentsPerGood = 10

> nAgents :: Nat
> nAgents = nGoods * nAgentsPerGood + 1

> nIters :: Nat
> nIters = 1000

prices

> priceRange :: (Price, Price)
> priceRange = (0.1, 1.0)

> pricesDist :: SimpleProb Price
> pricesDist = makeDiscreteUniformSP priceRange (nInitialPrices - 1)

goods

> goods :: [Good]
> goods = [0..(nGoods - 1)]

scarf utility weights

> suw :: [(Good, Quantity)]
> suw = [(g, 1.0) | g <- goods]

schedule

> nTradesPerPeriod :: Nat
> nTradesPerPeriod = 10

> sch :: Schedule
> sch = concat [[[Trade], [Consume]] | k <- [1..nTradesPerPeriod]] 
>       ++
>       [[Produce], [Learn], [Mutate], [Idle]]

mutation parameters

> mutRate :: REAL
> mutRate = 0.03

> mutPricesDist :: SimpleProb Price
> mutPricesDist = pricesDist


Setup:

population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(SomeGoodsScarfConsumer g suw, nAgentsPerGood) |
>                    g <- goods]
>                   ++
>                   [(Timekeeper, 1)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, SomeGoodsScarfConsumer myg sgqs) iks
> --- require nInitialPrices > 1
>     = makeSomeGoodsScarfConsumer i iks myg sgqs gps stock mp sch
>     where gps = [(g, unsafeRandomize pricesDist) | g <- goods]
>           stock = [(myg, eval sgqs myg)]
>           mp = (mutRate, mutPricesDist)

> agentConstructionScheme (i, Timekeeper) iks
>     = makeTimekeeper i iks 0

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme


Iterate agents and print trajectories

> agentsToBool :: Agents -> Bool
> agentsToBool ags = (tif == "True")
>     where tif = showAgent "time incremented flag" (ags!!(nAgents - 1))

> cmd :: String
> cmd = "prices"
> -- cmd = "goodsAndQuantities"

> agentsToString :: Agents -> String
> agentsToString ags = out
>     where out = "time: "
>                 ++
>                 showAgent "time" (ags!!(nAgents - 1))
>                 ++
>                 "; "
>                 ++
>                 cmd ++ ": " 
>                 ++
>                 show (map (++ "  ") (map (showAgent cmd) ags))



> main = printTrj  agentsToBool agentsToString ags nIters

--> ags' = iteratelAgent ags 3


