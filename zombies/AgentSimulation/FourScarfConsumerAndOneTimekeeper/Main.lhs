#!/usr/bin/runhugs -98

> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import NumericTypes.Real
> import Util.List.Ops
> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Unsafe.Random.Ops

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule
> import Timekeepers.Timekeeper

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsId = [AgentId]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters


> nIter :: Nat
> nIter = 80


Setup population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(ScarfConsumer 0 [(0, 5.0), (1, 10.0)], 2), 
>                    (ScarfConsumer 1 [(0, 5.0), (1, 10.0)], 2),
>                    (Timekeeper, 1)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> sch :: Schedule
> sch = [[Produce], [Trade], [Consume], [Learn], [Idle]]

> spPrice :: Good -> SimpleProb REAL
> spPrice g = makeDiscreteUniformSP (1.0, 3.0) 2

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, ScarfConsumer 0 sgqs) iks
>     = makeScarfConsumer i iks 0 sgqs gps [] sch
>     where gps = [(g, unsafeRandomize (spPrice g)) | g <- goods]

> agentConstructionScheme (i, ScarfConsumer 1 sgqs) iks
>     = makeScarfConsumer i iks 1 sgqs gps [] sch
>     where gps = [(g, unsafeRandomize (spPrice g)) | g <- goods]

> agentConstructionScheme (i, Timekeeper) iks
>     = makeTimekeeper i iks 0

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme


Iterate agents and print trajectories

> agentsToString :: Agents -> String
> agentsToString [c0, c1, c2, c3, tk] =
>     (showAgent "time" tk) ++ ";         " ++
>     "c0, " ++ (showAgent "utility" c0) ++ "; " 
>            ++ (showAgent "prices" c0) ++ "; "
>            ++ (showAgent "goodsAndQuantities" c0) ++ ";         " ++
>     "c1, " ++ (showAgent "utility" c1) ++ "; " 
>            ++ (showAgent "prices" c1) ++ "; "
>            ++ (showAgent "goodsAndQuantities" c1) ++ ";         " ++
>     "c2, " ++ (showAgent "utility" c2) ++ "; " 
>            ++ (showAgent "prices" c2) ++ "; "
>            ++ (showAgent "goodsAndQuantities" c2) ++ ";         " ++
>     "c3, " ++ (showAgent "utility" c3) ++ "; " 
>            ++ (showAgent "prices" c3) ++ "; "
>            ++ (showAgent "goodsAndQuantities" c3)



--> agentsToString :: Agents -> String
--> agentsToString [c0, c1, c2, c3, tk] =
-->     (showAgent "time" tk) ++ ";         " ++
-->     "c0, " ++ (showAgent "" c0) ++ "; " ++
-->     "c1, " ++ (showAgent "" c1) ++ "; " ++
-->     "c2, " ++ (showAgent "" c2) ++ "; " ++
-->     "c3, " ++ (showAgent "" c3) ++ "; " 


> main = printTrj agentsToString ags nIter



