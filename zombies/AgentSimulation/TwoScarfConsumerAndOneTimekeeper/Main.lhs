#!/usr/bin/runhugs -98

> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import Util.List.Ops

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule
> import Timekeepers.Timekeeper

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsId = [AgentId]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters


> nIter :: Nat
> nIter = 100


Setup population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(ScarfConsumer 0 [(0, 5.0), (1, 10.0)], 1), 
>                    (ScarfConsumer 1 [(0, 5.0), (1, 10.0)], 1),
>                    (Timekeeper, 1)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> sch :: Schedule
> sch = [[Produce], [Trade], [Consume], [Idle]]

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, ScarfConsumer 0 sgqs) iks
>     = makeScarfConsumer i iks 0 sgqs gps [] sch
>     where gps = [(0, 1.0), (1, 3.0)]

> agentConstructionScheme (i, ScarfConsumer 1 sgqs) iks
>     = makeScarfConsumer i iks 1 sgqs gps [(1, 1.0)] sch
>     where gps = [(0, 1.0), (1, 2.0)]

> agentConstructionScheme (i, Timekeeper) iks
>     = makeTimekeeper i iks 0

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme


Iterate agents and print trajectories

> agentsToString :: Agents -> String
> agentsToString [c0, c1, tk] = 
>     (showAgent "time" tk) ++ "; " ++
>     "c0, " ++ (showAgent "goodsAndQuantities" c0) ++ "; " ++
>     "c1, " ++ (showAgent "goodsAndQuantities" c1)

> main = printTrj agentsToString ags nIter



