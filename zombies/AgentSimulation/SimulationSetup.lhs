> module AgentSimulation.SimulationSetup where

> import NumericTypes.Nat
> import Prob.SimpleProb
> import Util.Unsafe.Random.Ops
> import Agent.Agent


Setup list of agent kinds

> plainSetupKinds :: [(k, Nat)] -> [k]
> plainSetupKinds kns = concat [replicate n k | (k, n) <- kns] 

> unsafeRandomSetupKinds :: Nat -> SimpleProb k -> [k]
> unsafeRandomSetupKinds n sp = [unsafeRandomize sp | i <- [1..n]]


Setup network relation (list of lists of Id and Kind pairs)

> setupNet :: [k] -> 
>             ((Nat, k) -> (Nat, k)-> Bool) -> 
>             [[(Nat, k)]]
> setupNet ks p = [[(j, ks!!j) | j <- [0..n], 
>                                p (i, ks!!i) (j, ks!!j) == True] 
>                  | i <- [0..n]]
>     where n = length ks - 1

> setupEmptyNet :: [k] -> [[(Nat, k)]]
> setupEmptyNet ks = [[] | k <- ks]

> setupAllToAllNet :: [k] -> [[(Nat, k)]]
> setupAllToAllNet ks = setupNet ks p
>     where p (i, ki) (j, kj) = True

> setupAllToAllOthersNet :: [k] -> [[(Nat, k)]]
> setupAllToAllOthersNet ks = setupNet ks p
>     where p (i, ki) (j, kj) = (i /= j)

> setupAllToAllSameKindNet :: (Eq k) => [k] -> [[(Nat, k)]]
> setupAllToAllSameKindNet ks = setupNet ks p
>     where p (i, ki) (j, kj) = (ki == kj)

> setupAllToAllOthersSameKindNet :: (Eq k) => [k] -> [[(Nat, k)]]
> setupAllToAllOthersSameKindNet ks = setupNet ks p
>     where p (i, ki) (j, kj) = (i /= j && ki == kj)


Setup list of agents

> setupAgents :: [k] ->
>                [[(Nat, k)]] ->
>                ((Nat, k) -> [(Nat, k)] -> Agent m) ->
>                [Agent m]

> setupAgents ks ikss disp = [disp (i, ks!!i) (ikss!!i) | i <- [0..n]]
>     where n = length ks - 1



