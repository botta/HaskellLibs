> module Env where

> import List
> import NumericTypes.Nat
> import NumericTypes.Real
> import Agent.Agent



> type Time = Nat

> type AgentId = Nat

> type Quantity = REAL


> type Price = REAL


> type Utility = REAL


> type Good = Nat


> type Trade = [(Good, Quantity)] 


> data AgentKind = Unknown 
>                | Master
>                | ScarfConsumer Good [(Good,Quantity)]
>                | DaltonScarfConsumer Good [(Good,Quantity)]
> 		 | SomeGoodsScarfConsumer Good [(Good,Quantity)] 
>                | Timekeeper
>                  deriving (Eq, Show)


> data RR = Observe
>         | Reveal Utility [(Good, Price)]
>         | Req_Kind
>         | Rep_Kind AgentKind  
>         | Offer [Trade] 
>         | Accept Trade
>	  | MustRevealUtility
>	  | MustMutate
>	  | StillToTrade [(AgentId,Good)]
>	  | Done
>         | AreYouIdle
>         | IdlePositive
>         | IdleNegative
>         | AreAllIdle
>         | AllIdlePositive
>         | AllIdleNegative
>           deriving (Eq, Show)


> type Msg = (RR, AgentId)

> isOffer :: Msg -> Bool
> isOffer (Offer tdgqss, k) = True
> isOffer _ = False

> isAccept :: Msg -> Bool
> isAccept (Accept tsgqs, k) = True
> isAccept _ = False

> isReveal :: Msg -> Bool
> isReveal (Reveal u gps, k) = True
> isReveal _ = False

> isIdlePositive :: Msg -> Bool
> isIdlePositive (IdlePositive, k) = True
> isIdlePositive _ = False

> goods :: [Good]
> goods = [0,1,2]


