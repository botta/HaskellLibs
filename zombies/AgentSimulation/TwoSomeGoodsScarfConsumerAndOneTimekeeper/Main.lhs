#!/usr/bin/runhugs -98

> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import Util.List.Ops

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule
> import Timekeepers.Timekeeper

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsId = [AgentId]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters


> nIter :: Nat
> nIter = 20


Setup population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(SomeGoodsScarfConsumer 0 [(0, 5.0), (1, 10.0),
> 		    			       	    	   (2,20.0)], 1), 
>                    (SomeGoodsScarfConsumer 1 [(0, 5.0), (1, 10.0), 
>		     			       	    	  (2,20.0)], 1),
>                    (Timekeeper, 1)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> sch :: Schedule
> sch = [[Produce], [Consume], [Trade], [Idle]]

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, SomeGoodsScarfConsumer 0 sgqs) iks
>     = makeSomeGoodsScarfConsumer i iks 0 sgqs gps [(0,3.0)] sch
>     where gps = [(0, 1.0), (1, 3.0),(2,2.0)]

> agentConstructionScheme (i, SomeGoodsScarfConsumer 1 sgqs) iks
>     = makeSomeGoodsScarfConsumer i iks 1 sgqs gps 
>     				   [(0,5.0),(2,20.0), (1, 1.0)] sch
>     where gps = [(0, 1.0), (1, 2.0),(2,2.0)]

> agentConstructionScheme (i, Timekeeper) iks
>     = makeTimekeeper i iks 0

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme


Iterate agents and print trajectories

> agentsToString :: Agents -> String
> agentsToString [c0, c1, tk] = 
>     (showAgent "time" tk) ++ "; " ++
>     "c0, " ++ (showAgent "" c0) ++ "; " ++
>     "c1, " ++ (showAgent "" c1)

> main = printTrj agentsToString ags nIter

> agentsToString' :: Agents -> String
> agentsToString' [c0, c1, tk] = 
>     (showAgent "time" tk) ++ "; " ++
>     "c0, " ++ (showAgent "goodsAndQuantities" c0) ++ "; " ++
>     "c1, " ++ (showAgent "goodsAndQuantities" c1)

> main' = printTrj agentsToString' ags nIter

