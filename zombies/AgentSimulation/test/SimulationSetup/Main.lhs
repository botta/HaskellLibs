> module Main where

> import Agent.Agent
> import NumericTypes.Nat
> import Util.List.Ops
> import Prob.SimpleProb
> import Prob.SimpleProbOps

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Master
> import EconomicAgents.Action
> import EconomicAgents.Schedule

> import AgentSimulation.SimulationSetup

> import Env


Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Setup population, kinds, network 

ScarfConsumer g sgqs must satisfy
sort (map fst ( filter (snd>0) (normalize sgqs)) == goods
  

> agentPopulation :: AgentPopulation
> agentPopulation = [(Master, 1), 
>                    (ScarfConsumer 0 [(0,10),(1,20)], 2),
>                    (ScarfConsumer 1 [(0,10),(1,20)], 3)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR


gps must satisfy  
sort (map fst (filter (snd >0) (normalize gps)) == goods

> agentConstructionScheme (i, ScarfConsumer g sgqs) iks
>     = makeScarfConsumer i iks g sgqs gps [] sch
>       where gps = [(0,1.0),(1,2.0)]
>             sch = [[Idle]]

> agentConstructionScheme (i, Master) iks
>     = makeMaster i iks

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme

> main = printList (map (showAgent "") ags)

