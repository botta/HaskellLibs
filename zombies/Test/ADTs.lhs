> module ADTs where


> data A0 = forall s . A0 (s -> (Int, s)) s

> data A1 = forall s . A1 (s -> ((Int, s), Char)) s


> f :: A0 -> (Int, A0)
> f (A0 ops s) = (n, A0 ops s')
>     where (n, s') = ops s

> a0 = A0 (\n -> (n, n + 1)) 0

> a1 = A1 (\n -> ((n + 1, n + 2), 'd')) 1

> a1ToA0 :: A1 -> A0
> a1ToA0 (A1 ops s) = A0 (fst . ops) s





