> module FHHAgents.WorkingH where

> import Agent.Agent

> import Env

> data Status =  Normal | Waiting deriving (Eq, Show)

> type Val    =  (Status,
>                GoodsPerLabour,               --  LabourOffer
>                GoodsPerLabour)               --  LabourProductivity

> type State = (AgentId, 
>               [(AgentId, AgentKind)], 
>               Val,
>               [Msg])


Constructors

> makeWorkingH :: AgentId -> 
>                   [(AgentId, AgentKind)] -> 
>                   Val -> 
>                   Agent RR
> makeWorkingH i iks val = Agent ident
>                                outMsgs
>                                step
>                                showWorkingH
>                                (i, iks, val)

> ident :: State -> AgentId
> ident (i, iks, val, outs) = i

> outMsgs :: State -> [Msg] 
> outMsgs (i, iks, val, outs) = outs

> step :: State -> [Msg] -> State
> step s ins = s

> data State = WS Val
>                 Working2Firm Working2Rich
>              deriving (Eq, Show)

> data Input = WI Firm2Working Rich2Working

> transition :: State -> Input -> State

The working household keeps transmitting its offer to the firm,
in order for the cycle to resume.  Instead, what would be needed
is a way of restarting the cycle without having these artificial 
initial offers.

> transition (WS (Normal, labourOffer, labourProductivity) _ _) 
>            (WI _ Wait)
>            = WS (Waiting, labourOffer, labourProductivity) 
>              (Offer labourOffer) NoMsgW2R

> transition (WS (Waiting, labourOffer, labourProductivity) _ _)
>            (WI _ Wait)
>            = WS (Waiting, labourOffer, labourProductivity) 
>              (Offer labourOffer) NoMsgW2R


> transition (WS (Waiting, labourOffer, labourProductivity) _ _)
>            (WI _ NoMsgR2W)
>            = WS (Normal, labourOffer, labourProductivity)
>              (Offer labourOffer) NoMsgW2R

> transition (WS (Normal, labourOffer, labourProductivity) _ _)
>            (WI (WageOffer wage jobs) NoMsgR2W)
>            =
>            WS (Normal, labourOffer', labourProductivity')
>            (Offer labourOffer') NoMsgW2R
>            where
>            labourOffer'        =  lOfferFun wage 
>            labourProductivity' =  lProdFun labourProductivity jobs
>            lOfferFun w         =  w * 3.0
>            lProdFun lp j       =  lp * j * 1.0

It would appear we need an initial offer: potential for errors!

> initialState  ::  State
> initialState   =  WS (Normal, 1.0, 1.0) (Offer 1.0) NoMsgW2R
