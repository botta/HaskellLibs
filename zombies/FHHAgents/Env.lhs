> module FHHAgents.Env where

> import List
> import NumericTypes.Nat
> import NumericTypes.Real
> import Agent.Agent

>  type Real            =  Double
>  type PReal           =  Double

>  type GoodsStock      =  PReal
>  type LabourFlow      =  PReal
>  type GoodsPerLabour  =  PReal
>  type GoodsFlow       =  Real

-->  data TGoodsFlow      =  Td GoodsFlow | Nil

> data RR = NoMsg 
>         | Offer LabourFlow
>         | Wait 
>         | Ask 
>         | Withdraw GoodsFlow
>         | WageOffer GoodsPerLabour LabourFlow
>         | ReportProfit GoodsFlow 
>         | ReportWithdrawal GoodsFlow 
>           deriving (Eq, Show)

> type Msg = (RR, AgentId)
