> module NumericTypes.Real(REAL, average, eval, interpolate) where

> import Ord.Ord


> type REAL = Double


> average :: [REAL] -> REAL
> average xs = let lxs = length xs in
>              if lxs == 0
>              then 0
>              else (sum xs) / (fromIntegral lxs)


> eval :: [(REAL, REAL)] -> REAL -> REAL
> eval xys x = let ys = [y' | (x',y') <- xys, x' == x] in
>              if null ys
>              then error "eval: null list."
>              else average ys


> interpolate :: [(REAL, REAL)] -> REAL -> REAL
> interpolate xys x = 
>   if null xys
>   then error "interpolate: null list."
>   else let xs = map fst xys in  
>        case maxLE xs x of
>          Nothing -> eval xys (minimum xs)
>          Just x1 -> case minGE xs x of
>                       Nothing -> eval xys (maximum xs)
>                       Just x2 -> if x1 == x2
>                                  then eval xys x1
>                                  else let y1 = eval xys x1 in
>                                       let y2 = eval xys x2 in
>                                       y1 + (x - x1) / (x2 - x1) * (y2 - y1)