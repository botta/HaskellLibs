> module Prob.SimpleProbOps(makeConcentratedSP,
>                           makeUniformSP,
>                           makeConvCombSP,
>                           makeInitsSP,
>                           rescaleSP,
>                           makeDiscreteUniformSP,
>                           unwrapSP,
>                           evalSP,
>                           levalSP,
>                           mostProbs,
>                           chi2) where

> import Prob.SimpleProb
> import NumericTypes.Nat
> import NumericTypes.Real
> import Util.List.Ops
> import List


Construction

> makeConcentratedSP :: a -> SimpleProb a
> makeConcentratedSP = return

> makeUniformSP :: [a] -> SimpleProb a
> makeUniformSP xs = SP [(x, p) | x <- xs]
>     where p = 1.0 / (realToFrac (length xs))

> makeConvCombSP :: Prob -> 
>                   SimpleProb a -> 
>                   SimpleProb a -> 
>                   SimpleProb a
> makeConvCombSP t sp1 sp2 = SP xps
>     where sp1' = rescaleSP sp1 t
>           sp2' = rescaleSP sp2 (1.0 - t)
>           xps = (toList sp1') ++ (toList sp2')

> makeInitsSP :: SimpleProb a -> SimpleProb [a]
> makeInitsSP (SP xps) = SP (zip ixs ips)
>     where ixs = inits (map fst xps)
>           ips = map sum (inits (map snd xps))

> rescaleSP :: SimpleProb a -> Prob -> SimpleProb a
> rescaleSP (SP xps) t = SP [(x, p * t) | (x, p) <- xps]

> makeDiscreteUniformSP :: (REAL, REAL) -> Nat -> SimpleProb REAL
> makeDiscreteUniformSP (a, b) n = SP [(x, p) | x <- xs]
> --- require a < b
> --- require n > 0
>     where xs = [a + (realToFrac k) * (b - a) / (realToFrac n) | k <- [0..n]]
>           p = 1.0 / (realToFrac (length xs))


Unwrapping

> unwrapSP :: SimpleProb a -> [(a, Prob)]
> unwrapSP (SP xps) = xps


Evaluation

> evalSP :: Eq a => SimpleProb a -> a -> Prob
> evalSP (SP xps) x = sum [p | (x', p) <- xps, x' == x]

> levalSP :: Eq a => SimpleProb a -> [a] -> Prob
> levalSP sp xs = sum [evalSP sp x | x <- xs]

> mostProbs :: Eq a => SimpleProb a -> [a]
> mostProbs (SP xps) = fst (foldr f ([], 0.0) (normalize xps))
>     where f (x', p') (xs, p) = if (p > p')
>                                then (xs, p)
>                                else if (p == p')
>                                     then ((x':xs), p)
>                                     else ([x'], p')


Chi-Square (Pearson)

> chi2 :: (Eq a) => SimpleProb a -> [a] -> REAL
> chi2 xps xs' = sum rs
>     where rs = [(((eval fxs x) - (eval fxs' x)))
>                 *
>                 (((eval fxs x) - (eval fxs' x)))
>                 /
>                 (eval fxs x)
>                 | x <- supp xps]
>           fxs = rescale (unwrapSP xps) n
>           fxs' = [(x, 1) | x <- xs']
>           n = realToFrac (length xs')



