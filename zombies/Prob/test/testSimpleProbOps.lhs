> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Unsafe.Random.Ops
> import NumericTypes.Nat
> import Util.List.Ops


> sp0 = makeUniformSP [-1, 1]
> sp1 = makeUniformSP [0]
> sp2 = makeConvCombSP 0.5 sp0 sp1
> spisp2 = makeInitsSP sp2
> xps = unwrapSP sp0
> p = evalSP sp0 1

> nx =10
> sp = makeUniformSP [1..nx]
> no = 100000
> xs' = [unsafeRandomize sp | k <- [1..no]]
> c2 = chi2 sp xs'

> n = realToFrac (length xs')
> fxs' = [(x, 1.0) | x <- xs']
> fxs = rescale (unwrapSP sp) n

> rs' = [(eval fxs x) - (eval fxs' x)
>        | x <- supp sp]

> rs = [(((eval fxs x) - (eval fxs' x)))
>        *
>        (((eval fxs x) - (eval fxs' x)))
>       /
>       (eval fxs x)
>       | x <- supp sp]

<<<<<<< .mine
=======
> dsp = makeDiscreteUniformSP (1.2, 7.4) 10

> lala = mostProbs (SP [(1, 0.15), (-1, 0.25), (2, 0.0), (-1, 0.05), (1, 0.25), (3, 0.3)])

<<<<<<< .mine
>>>>>>> .r179
=======

>>>>>>> .r182
