%if not lhs2tex_lhs2tex_fmt_read
%include fmts.fmt
%endif

%if False

> module Prob.SimpleProb where

> import List
> import Util.Ops
> import NumericTypes.Real

%endif

\subsection{Simple probability distributions}
\label{subsec:spd}

A simple probability distribution is one with a finite (or at most
countable) support, so what we want is something like the following:

< type Supp a = [a]

to represent the support, and 

> type Prob = REAL

< newtype SimpleProb a = SP (a -> Prob, Supp a)

to represent the distribution.  Unfortunately, we can not declare
this new type to be an instance of the monad class, since we can
only define the |return| and |(>>=)| operations on types which are
instances of |Eq|.  (For a similar reason, the Haskell implementation
of the powerset functor, |Set|, cannot be declared to be an instance
of the |Monad| type class.)

Therefore, we shall adopt the representation used by Erwig in
\cite{Erwig:pfp}:

> newtype SimpleProb a = SP [(a, Prob)] deriving Show

> toList        :: SimpleProb a -> [(a, Prob)]
> toList (SP ds) = ds

The support of a simple probability distribution |ds| is the set of
values in the list |map fst ds|, but now we need to ensure that there
are no values associated to zero probability:

> supp        :: Eq a => SimpleProb a -> [a]
> supp (SP ds) = nub (map fst (filter notz ds))
>                where 
>                notz (x, p) = p /= 0

The main difference between the list-based representation and the
functional one is that we can now have several values associated
with the same element, and values associated to zero probability
(which are therefore outside the support).  This makes it necessary
to introduce a function that normalizes the representation:

> normalizeSP :: Eq a => SimpleProb a -> SimpleProb a
> normalizeSP (SP ds) = SP (map (pair (id, f)) (supp (SP ds)))
>                       where
>                       f a = sum [p | (x, p) <- ds, x == a]

We can now define |SimpleProb| as an instance of the typeclasses |Eq|
and |Monad|.

> instance Eq a => Eq (SimpleProb a) where
>   sp1 == sp2 = length ps1 == length ps2 &&
>                and (map (`elem` ps2) ps1)
>                where 
>                ps1 = toList (normalizeSP sp1)
>                ps2 = toList (normalizeSP sp2)

> instance Monad SimpleProb where
>   return a = SP [(a, 1.0)]
>   SP (ds1) >>= f = SP (concat (map g ds1))
>                    where
>                    g (a, p) = map h (toList (f a))
>                               where
>                               h (x, p') = (x, p'*p)
>                    

The monadic bind operator expresses the conditional probabilities of
the elements in the target of |f|, depending on the distribution on
the elements in the source of |f|.  For finite, identical source and
target, |f| can be represented as a stochastic matrix, and the bind
operator gives the transition of the associated markov chain.

%if False

-- > sp1 :: SimpleProb Int
-- > sp1  = SP [(-1, 0.5), (1, 0.5)]

-- > sp2 :: SimpleProb Int
-- > sp2  = SP [(1, 0.5), (-1, 0.5), (2, 0)]

-- > apply :: Eq x => SimpleProb x -> x -> Prob
-- > apply (SP px) x = sum [p | (x', p) <- px, x' == x]

%endif
