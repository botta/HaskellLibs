> module ThreeArmedBandit where

> import Agent
> import List (sortBy)


Arms, counters and rewards

> data Arm = A | B | C deriving (Eq, Show)
> type Counter = Int
> type Reward = Float


The player type 

> type Player = Arm -> (Counter,Reward)


The three-armed bandit type

> type Bandit = Arm -> Reward


'Player' is an 'Agent'

> instance Agent Player where
>   type Environment Player = Bandit
>   type State Player = Player
>   type Action Player = Arm
>   type Reaction Player = Reward
>   observe p _ = p
>   select p _ =  a
>     where ((a,_,_):_) = zeroPulls ++ sortDesc ucb m
>           zeroPulls   = filter ((==0) . pulls) m  
>           n           = fromIntegral (sum (map pulls m))
>           ucb (_,c,r) = r/ni + sqrt (log n/ni) 
>                           where ni = fromIntegral c
>           pulls (_,c,_) = c
>           m = [(a,fst (p a), snd (p a)) | a <- [A,B,C]]
>   adapt p _ a' r = \ a -> if (a == a') 
>                           then (fst (p a) + 1, snd (p a) + r) 
>                           else p a


'Bandit' is an AgentEnvironment

> instance AgentEnvironment Bandit where
>   type AgentAction Bandit = Arm
>   type AgentReaction Bandit = Reward
>   react b a = (b,b a)


Helper functions 

> sortDesc :: Ord b => (a -> b) -> [a] -> [a]
> sortDesc f = sortBy (\x y->compare (f y) (f x))

> pulls :: Player -> Arm -> Int
> pulls p a = fst (p a)

> rewards :: Player -> Arm -> Reward
> rewards p a = snd (p a)

> total_pulls :: Player -> Int
> total_pulls p = pulls p A + pulls p B + pulls p C

> total_rewards :: Player -> Reward
> total_rewards p = rewards p A + rewards p B + rewards p C

> pull_freq :: Player -> Arm -> Float
> pull_freq p a = (fromIntegral (pulls p a)) / (fromIntegral (total_pulls p))

> pull_freqs :: Player -> [Float]
> pull_freqs p = [pull_freq p a | a <- [A,B,C]]

                                                
Example:

Initial player and bandit

> p :: Player                                                                 
> p = \ a -> (0, 0.0)

> b :: Bandit
> b = \ a -> case a of
>              A -> 0.1  
>              B -> 0.2
>              C -> 0.3  

> m, n :: Int
> m = 10
> n = 10000

> lala = map (pull_freqs . fst) (drop (n - m) (take n (iterate step (p,b))))




