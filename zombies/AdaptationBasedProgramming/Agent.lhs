> module Agent where


One would like to write something like

-------------------------------------------------------------------
-- > class Agent a where                                         --
-- >   type State a                                              --
-- >   type Action a                                             --
-- >   type Reaction a                                           --
-- >   observe :: AgentEnvironment a e => a -> e > State a       --
-- >   select :: a -> State a -> Action a                        --
-- >   adapt :: a -> State a -> Action a -> Reaction a -> a      --
--                                                               --
-- > class Agent a => AgentEnvironment a e where                 --
-- >   react :: e -> Action a -> (e, Reaction a)                 --
--                                                               --
-- > step :: (Agent a, AgentEnvironment a e) => (a, e) -> (a, e) --
--                                                               --
-- > step (a,e) = (a',e')                                        --
-- >   where a' = adapt a state action reaction                  --
-- >         (e',reaction) = react e action                      --
-- >         action = select a state                             --
-- >         state = observe a e                                 --
-------------------------------------------------------------------

The problem here is the type of 'step':

-----------------------------------------------------------------------
-- > :l Agent                                                        --
-- [1 of 1] Compiling Agent            ( Agent.lhs, interpreted )    --
--                                                                   --
-- Agent.lhs:20:36:                                                  --
--     Couldn't match expected type `Reaction a1'                    --
--            against inferred type `Reaction a'                     --
--       NB: `Reaction' is a type function, and may not be injective --
--     In the fourth argument of `adapt', namely `reaction'          --
--     In the expression: adapt a state action reaction              --
--     In the definition of `a'': a' = adapt a state action reaction --
--                                                                   --
-- Agent.lhs:21:26:                                                  --
--     Could not deduce (AgentEnvironment a e)                       --
--       from the context (Agent a1, AgentEnvironment a1 e)          --
--       arising from a use of `react' at Agent.lhs:21:26-39         --
--     Possible fix:                                                 --
--       add (AgentEnvironment a e) to the context of                --
--         the type signature for `step'                             --
--     In the expression: react e action                             --
--     In a pattern binding: (e', reaction) = react e action         --
--     In the definition of `step':                                  --
--         step (a, e)                                               --
--                = (a', e')                                         --
--                where                                              --
--                    a' = adapt a state action reaction             --
--                    (e', reaction) = react e action                --
--                    action = select a state                        --
--                    state = observe a e                            --
--                                                                   --
-- Agent.lhs:21:34:                                                  --
--     Couldn't match expected type `Action a'                       --
--            against inferred type `Action a1'                      --
--       NB: `Action' is a type function, and may not be injective   --
--     In the second argument of `react', namely `action'            --
--     In the expression: react e action                             --
--     In a pattern binding: (e', reaction) = react e action         --
-- Failed, modules loaded: none.                                     --
-- Prelude>                                                          --
-----------------------------------------------------------------------

If one does not explicitely provide a type for 'step', the type system
deduces:

-------------------------------------
-- *Agent> :t step                 --
-- step                            --
--   :: forall a t t1.             --
--      (Reaction a ~ Reaction t,  --
--       Action t ~ Action a,      --
--       AgentEnvironment t t1,    --
--       AgentEnvironment a t1) => --
--      (t, t1) -> (t, t1)         --
-- *Agent>                         --
-------------------------------------

One possibility is to define 'Agent' and 'AgentEnvironment' as
independent type classes and explicitely enforce the relationships
between type families in the context:


> class Agent a where
>   type Environment a
>   type State a
>   type Action a
>   type Reaction a
>   observe :: a -> Environment a -> State a
>   select :: a -> State a -> Action a
>   adapt :: a -> State a -> Action a -> Reaction a -> a


> class AgentEnvironment e where
>   type AgentAction e
>   type AgentReaction e
>   react :: e -> AgentAction e -> (e, AgentReaction e)


> step :: (Agent a,
>          AgentEnvironment e,
>          Environment a ~ e,
>          Action a ~ AgentAction e,
>          Reaction a ~ AgentReaction e) => (a, e) -> (a, e)
> step (a,e) = (a',e')
>   where a' = adapt a state action reaction
>         (e',reaction) = react e action          
>         action = select a state
>         state = observe a e



> class Agent a => CoAgent a where
>   type CoAction a
>   type CoReaction a
>   coselect :: a -> State a -> CoAction a -> CoReaction a


> costep :: (CoAgent a,
>            CoAgent b,
>            Environment a ~  b,
>            Environment b ~  a,
>            Action a ~ CoAction b,
>            Action b ~ CoAction a,
>            Reaction a ~ CoReaction b,
>            Reaction b ~ CoReaction a) => (a,b) -> (a,b)
> costep (a,b) = (a',b')
>   where a' = adapt a sa aa crb
>         b' = adapt b sb ab cra
>         sa = observe a b
>         sb = observe b a
>         aa = select a sa
>         ab = select b sb
>         crb = coselect b sb aa
>         cra = coselect a sa ab
