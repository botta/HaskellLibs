> module AdaptationBasedProgramming.RLAgent where


> class RLAgent a where
>   type Action a
>   type Reaction a
>   policy :: a -> Action a
>   update :: a -> Reaction a -> a

'update' should possibly be a function but 'policy' shouldn' t. Policies
usually require actions (controls) to be drawn randomly (exploration).

> step :: RLAgent a => (Action a -> Reaction a) -> a -> [a]
> step f a = a:(step f a')
>   where a' = update a reaction
>         reaction = f action  
>         action = policy a


Example: the multi-armed bandit player

> type Arm = Int
> type Counter = Int
> type Reward = Float
> type MAB_Player = [(Arm,Counter,Reward)]

> instance RLAgent MAB_Player where
>   type Action MAB_Player = Arm
>   type Reaction MAB_Player = Reward
>   policy player = 1
>   update player _ = player

-- > mk_MAB_Player :: Int -> MAB_Player
-- > mk_MAB_Player n = [(i,0,0.0) | i <- [1..n]]

-- > ps :: [MAB_Player]
-- > ps = take 3 (step (\ a -> 1.0) (mk_MAB_Player 3))



