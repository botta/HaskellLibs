> module MarketsAndCoalitions.CoalitionAsTree(
>   single,
>   in_market,
>   immediate,
>   coalition) where



> import List
> import Util.SetOps.SetOps
> import Util.List.List
> import Util.Set.Set
> import MarketsAndCoalitions.Coalition


> data CoalitionTree a = Single a 
>                  | InMarket [CoalitionTree a]
>                  | Immediate [CoalitionTree a]


> instance (Eq a) => Eq (CoalitionTree a) where
>   Single c     == Single c'     = c == c'
>   InMarket cs  == InMarket cs'  = setify cs == setify cs'
>   Immediate cs == Immediate cs' = setify cs == setify cs'
>   _            == _ = False


> instance (Show a) => Show (CoalitionTree a) where
>   show (Single a) = show a
>   show (InMarket cas) = "(" ++ (concat (map show cas)) ++ ")"
>   show (Immediate cas) = "[" ++ (concat (map show cas)) ++ "]"


> ct_tops :: CoalitionTree a -> [CoalitionTree a]
> ct_tops (Single a) = [Single a]
> ct_tops (InMarket cas) = cas
> ct_tops (Immediate cas) = cas


> ct_singles :: CoalitionTree a -> [CoalitionTree a]
> ct_singles (Single a) = [Single a]
> ct_singles (InMarket cas) = concat (map ct_singles cas)
> ct_singles (Immediate cas) = concat (map ct_singles cas)


> ct_in_markets :: CoalitionTree a -> [CoalitionTree a]
> ct_in_markets (Single a) = []
> ct_in_markets (InMarket cas) = (InMarket cas):(concat (map ct_in_markets cas))
> ct_in_markets (Immediate cas) = concat (map ct_in_markets cas)


> ct_immediates :: CoalitionTree a -> [CoalitionTree a]
> ct_immediates (Single a) = []
> ct_immediates (InMarket cas) = concat (map ct_immediates cas)
> ct_immediates (Immediate cas) = (Immediate cas):(concat (map ct_immediates
>                                                       cas))


> first_coalitioner :: CoalitionTree a -> a
> first_coalitioner (Single a) = a
> first_coalitioner (InMarket (c:cs)) = first_coalitioner c
> first_coalitioner (Immediate (c:cs)) = first_coalitioner c


> coalitioners :: CoalitionTree a -> [a]
> coalitioners (Single a) = [a]
> coalitioners (InMarket cas) = concat (map coalitioners cas)
> coalitioners (Immediate cas) = concat (map coalitioners cas)


> normalize :: (Ord a) => CoalitionTree a -> CoalitionTree a
> normalize (Single a) = Single a
> normalize (InMarket cs) = InMarket cs' where
>   cs' = sortBy f cs''
>   f c c' | first_coalitioner c == first_coalitioner c' = EQ
>          | first_coalitioner c <  first_coalitioner c' = LT
>          | otherwise = GT
>   cs'' = map normalize cs
> normalize (Immediate cs) = Immediate cs' where
>   cs' = sortBy f cs''
>   f c c' | first_coalitioner c == first_coalitioner c' = EQ
>          | first_coalitioner c <  first_coalitioner c' = LT
>          | otherwise = GT
>   cs'' = map normalize cs


    
> single :: a -> CoalitionTree a
> single a = Single a



> in_market :: (Eq a) => [CoalitionTree a] -> CoalitionTree a
> in_market cts
>   | is_empty cts = 
>       error "in_market: argument: empty list."
>   | is_singleton cts = 
>       error "in_market: argument: singleton list."
>   | not (are_pairwise_disjoint (map coalitioners cts)) =
>       error "in_market: argument: coalitioners not pairwise disjoint."
>   | otherwise = InMarket cts


> immediate :: (Eq a) => [CoalitionTree a] -> CoalitionTree a
> immediate cts
>   | is_empty cts = 
>       error "immediate: argument: empty list."
>   | is_singleton cts = 
>       error "immediate: argument: singleton list."
>   | not (are_pairwise_disjoint (map coalitioners cts)) =
>       error "immediate: argument: coalitioners not pairwise disjoint."
>   | otherwise = (Immediate cts)


> coalition :: (Ord a, Show a) => CoalitionTree a -> Coalition a
> coalition ct = 
>   Coalition 
>   (show . normalize) 
>   ((map coalition) . ct_tops) 
>   ((map coalition) . ct_singles) 
>   ((map coalition) . ct_in_markets) 
>   ((map coalition) . ct_immediates) 
>   ct



Test:


> data Player = C | E | F | I | J | U deriving (Eq, Ord, Show)

> c, c' :: CoalitionTree Player

> c = immediate [immediate [single E, single J], 
>                single C,
>                in_market [single F, single I]]

> c' = immediate [immediate [single J, single I], 
>                 in_market [single F, single I],
>                 single C]




