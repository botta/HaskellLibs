> module MarketsAndCoalitions.MarketAsList(market) where


> import Util.SetOps.SetOps
> import MarketsAndCoalitions.Coalition
> import MarketsAndCoalitions.Market


> market :: [Coalition a] ->  Market a
> market cs
>   | is_empty cs = 
>       error "market: argument: empty list."
>   | (not (is_singleton cs))
>     &&
>     (not (is_empty (concat (map immediates cs)))) = 
>       error "market: argument: contains immediates."
>   | otherwise = Market id cs