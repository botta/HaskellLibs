> module MarketsAndCoalitions.Market where



> import List
> import MarketsAndCoalitions.Coalition



Market ADT:


> data Market a = forall t . Market (t -> [Coalition a]) 
>                                   t


Market ADT: auxiliary functions (core)


> top_level_coalitions :: Market a -> [Coalition a]
> top_level_coalitions (Market top_level_coalitions t) 
>   =
>   top_level_coalitions t


Market ADT: registrations


> instance Show (Market a) where
>   show m = concat (intersperse ['-'] (map show (top_level_coalitions m)))
>     


> instance Eq (Market a) where
>   m == m' = tlc \\ tlc' == tlc' \\ tlc where
>     tlc = top_level_coalitions m
>     tlc' = top_level_coalitions m'



