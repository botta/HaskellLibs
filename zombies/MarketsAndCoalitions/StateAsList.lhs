> module MarketsAndCoalitions.StateAsList(makeState) where


> import MarketsAndCoalitions.Market
> import MarketsAndCoalitions.State


> type StateAsList a = [Market a]


> makeState :: StateAsList a -> State a
> makeState ms = State id ms