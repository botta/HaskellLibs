> module MarketsAndCoalitions.ListListTreeState where

> import List
> import Util.Tree.Tree
> import MarketsAndCoalitions.State


> data P = A | B | C | D | E | F deriving (Eq, Ord, Show)

> type X = [[Tree P]]

> codeTree :: Tree P -> String 
> codeTree (Leaf p) = show p 
> codeTree (Twig pts) = "(" ++ (concat (map codeTree pts)) ++ ")"                                        

> codeListTree :: [Tree P] -> String
> codeListTree [] = ""
> codeListTree (t:[]) = codeTree t 
> codeListTree (t:t':ts) = codeTree t ++ "-" ++ codeListTree (t':ts)

> codeListListTree :: [[Tree P]] -> String
> codeListListTree [] = ""
> codeListListTree (ts:[]) = codeListTree ts 
> codeListListTree (ts:ts':tss) = codeListTree ts ++ "," ++ codeListListTree (ts':tss)





> p :: P
> p = C

> ps :: [P]
> ps = [C,A,F]

> x :: X
> x = [[Leaf A],[Leaf B],[Leaf C,Leaf F],[Leaf D],[Leaf E]]





