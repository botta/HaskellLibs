> module MarketsAndCoalitions.State where



> import List
> import MarketsAndCoalitions.Market



State ADT:


> data State a = forall t . State (t -> [Market a]) 
>                                 t



State ADT: auxiliary functions (core)


> markets :: State a -> [Market a]
> markets (State markets t) = markets t



State ADT: registrations


> instance Show (State a) where
>   show x = concat (intersperse [','] (map show (markets x)))


> instance Eq (State a) where
>   x == x' = ms \\ ms' == ms' \\ ms where
>     ms = markets x
>     ms' = markets x'



State ADT: axioms


1) Markets form a partition of P (each element of P belongs to exactly
one market):

  x :: State P =>
            
    1.1) setify (map traders (markets x)) = setify P

    1.2) m,m' elem (markets x), m != m', p elem m, p' elem m' => p != p'


2) Coalition hierarchy:

    2.1) p elem players

