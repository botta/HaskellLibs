> import Util.Set.Set
> import Util.SetOps.SetOps
> import MarketsAndCoalitions.Coalition
> import MarketsAndCoalitions.CoalitionAsTree
> import MarketsAndCoalitions.Market
> import MarketsAndCoalitions.MarketAsList
> import MarketsAndCoalitions.State
> import MarketsAndCoalitions.StateAsList



Test:

> data Player = C | E | F | I | J | U deriving (Eq, Ord, Show)

> c1, c2, c3 :: Coalition Player
> c1 = coalition (single C)
> c2 = coalition (in_market [in_market [single J, single E], single U])
> c3 = coalition (immediate [single I, single F])


> m1, m2 :: Market Player
> m1 = market [c1,c2]
> m2 = market [c3]

> x :: State Player
> x = makeState [m1,m2]

> sc2 = subs c2
> c' = head sc2
> c'' = head (tail sc2)

