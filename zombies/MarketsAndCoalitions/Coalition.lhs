> module MarketsAndCoalitions.Coalition where



> import Util.SetOps.SetOps



Coalition ADT:


> data Coalition a = forall t .  
>   Coalition (t -> String)         -- code
>             (t -> [Coalition a])  -- tops (signatories)
>             (t -> [Coalition a])  -- singles
>             (t -> [Coalition a])  -- in_markets
>             (t -> [Coalition a])  -- immediates
>             t


Coalition ADT: auxiliary functions (core)


> code :: Coalition a -> String
> code (Coalition code tops singles in_markets immediates t) 
>   = 
>   code t


> tops :: Coalition a -> [Coalition a]
> tops (Coalition code tops singles in_markets immediates t) 
>   = 
>   tops t


> singles :: Coalition a -> [Coalition a]
> singles (Coalition code tops singles in_markets immediates t) 
>   = 
>   singles t
  

> in_markets :: Coalition a -> [Coalition a]
> in_markets (Coalition code tops singles in_markets immediates t) 
>   = 
>   in_markets t
  

> immediates :: Coalition a -> [Coalition a]
> immediates (Coalition code tops singles in_markets immediates t) 
>   = 
>   immediates t
  

Coalition ADT: auxiliary functions


> subs :: Coalition a -> [Coalition a]
> subs c = singles c ++ in_markets c ++ immediates c


Coalition ADT: registrations


> instance Show (Coalition a) where
>   show = code


> instance Eq (Coalition a) where
>   c == c' = (code c) == (code c')


> instance Ord (Coalition a) where
>   compare c c' = compare (show c) (show c')






Coalition ADT: axioms


1) 

c :: Coalition a 

=> 

setify (singles c) 
== 
setify (in_markets c ++ immediates c >>= singles)


2) 

c :: Coalition a 

=>

c1 `subeq` c && c2 `subeq` c

=>

c1 `subeq` c2 || c2 `subeq` c1 || is_empty (intersection (subs c1) (subs c2))


3)

c :: Coalition a 

=>

c' `is_in` immediates c => not is_singleton (setify (singles c'))


4)

c :: Coalition a 

=>

c1 `is_in` immediates c && c2 `subeq` c && c1 `subeq` c2 => c2 `is_in` immediates c

