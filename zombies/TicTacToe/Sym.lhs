> module Sym where

    
> data Sym = X | O | E deriving (Eq, Show)


> type Tri = (Sym,Sym,Sym)

> isTriplet :: (Eq a) => a -> (a,a,a) -> Bool
> isTriplet a (x,y,z) = x == a && y == a && z == a

> isAlmostTriplet :: (Eq a) => a -> (a,a,a) -> Bool
> isAlmostTriplet a (x,y,z) = (x == a && y == a) || 
>                             (y == a && z == a) ||
>                             (z == a && x == a)

> findPosElem :: (a -> Bool) -> [a] -> (Int,a)
> findPosElem p as = lala p (zip [0..] as)
>   where lala :: (a -> Bool) -> [(Int,a)] -> (Int,a)
>         lala p [] = error "Element not found."
>         lala p (ia : ias) = if p (snd ia) then ia else lala p ias

> mkTri :: Sym -> Tri
> mkTri s = (s,s,s)

> xxx :: Tri
> xxx = mkTri X

> ooo :: Tri
> ooo = mkTri O






  
