> module Board where
  
> import Sym
> import Player


> type Board = [Sym]

> type Move = (Sym,Int)

> isValid :: Board -> Bool
> isValid b = length b == 9

> isEmpty :: Board -> Bool
> isEmpty b = if isValid b
>             then all (== E) b
>             else error "Invalid board."

> isFull :: Board -> Bool
> isFull b = if isValid b 
>            then not (any (== E) b)
>            else error "Invalid board."

> triples :: Board -> [Tri]
> triples x = if isValid x
>             then [(a,b,c),
>                   (d,e,f),
>                   (g,h,i),
>                   (a,d,g),
>                   (b,e,h),
>                   (c,f,i),
>                   (a,e,i),
>                   (g,e,c)]
>             else error "Invalid board." where
>   a = x!!0
>   b = x!!1
>   c = x!!2
>   d = x!!3
>   e = x!!4
>   f = x!!5
>   g = x!!6
>   h = x!!7
>   i = x!!8

> isWinner :: Player -> Board -> Bool
> isWinner p b = if isValid b
>                then any (isTriplet s) (triples b) 
>                else error "Invalid board." where
>   s = sym p

> isAlmostWinner :: Player -> Board -> Bool
> isAlmostWinner p b = if isValid b
>                      then any (isAlmostTriplet s) (triples b) 
>                      else error "Invalid board." where
>   s = sym p

> winningMove :: Player -> Board -> Move
> winningMove p b | not (isValid b) = error "Invalid board."
>                 | not (isAlmostWinner p b) = error "No winning move."
>                 | wtn == 0 && wt == (E,s,s) = (s,0)
>                 | wtn == 0 && wt == (s,E,s) = (s,1)
>                 | wtn == 0 && wt == (s,s,E) = (s,2)
>                 | wtn == 1 && wt == (E,s,s) = (s,3)
>                 | wtn == 1 && wt == (s,E,s) = (s,4)
>                 | wtn == 1 && wt == (s,s,E) = (s,5)
>                 | wtn == 2 && wt == (E,s,s) = (s,6)
>                 | wtn == 2 && wt == (s,E,s) = (s,7)
>                 | wtn == 2 && wt == (s,s,E) = (s,8)
>                 | wtn == 3 && wt == (E,s,s) = (s,0)
>                 | wtn == 3 && wt == (s,E,s) = (s,3)
>                 | wtn == 3 && wt == (s,s,E) = (s,6)
>                 | wtn == 4 && wt == (E,s,s) = (s,1)
>                 | wtn == 4 && wt == (s,E,s) = (s,4)
>                 | wtn == 4 && wt == (s,s,E) = (s,7)
>                 | wtn == 5 && wt == (E,s,s) = (s,2)
>                 | wtn == 5 && wt == (s,E,s) = (s,5)
>                 | wtn == 5 && wt == (s,s,E) = (s,8)
>                 | wtn == 6 && wt == (E,s,s) = (s,0)
>                 | wtn == 6 && wt == (s,E,s) = (s,4)
>                 | wtn == 6 && wt == (s,s,E) = (s,8)
>                 | wtn == 7 && wt == (E,s,s) = (s,6)
>                 | wtn == 7 && wt == (s,E,s) = (s,4)
>                 | wtn == 7 && wt == (s,s,E) = (s,2) where
>  wtn = fst wtnwt
>  wt  = snd wtnwt
>  s = sym p
>  wtnwt = findPosElem (isAlmostTriplet s) (triples b)

> move :: Move -> Board -> Board
> move (s,k) b | k < 0 || k > 8  = error "Invalid move (k < 0 || k > 8)."
>              | not (isValid b) = error "Invalid board."
>              | b!!k /= E       = error "Invalid move (b!!k /= E)."
>              | otherwise       = take k b ++ [s] ++ drop (k + 1) b



> b1 :: Board
> b1 = [E,E,E,
>       E,E,E,
>       E,E,E]

> b2 :: Board
> b2 = [E,X,X,
>       O,O,E,
>       O,X,E]










  
