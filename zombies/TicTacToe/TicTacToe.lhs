> module TicTacToe where

> import IO

> import Sym
> import Board
> import Player

> type Box = (Board, [Move])

> type Mem = [Box]

> b0 :: Board
> b0 = replicate 9 E

> memory :: (Player, Board, Mem) -> Mem
> memory (_, _, m) = m


> agame :: (Player, Board, Mem) -> (Player, Board, Mem)
> agame pbm = pbm








> game :: (Player, Board, Mem) -> IO (Player, Board, Mem)
> game pbm = do
>            

             ...
  
>            return 

> stop :: IO ()
> stop = return ()

> play :: Mem -> IO ()
> play mem = do 
>            putStrLn "New game, your turn"  

             ...

>            pbm <- game (PX, b0, mem)         

             ...
                    
>            putStrLn "Another game ?"
>            c <- getChar
>            if c == 'n' 
>            then stop 
>            else play (memory pbm)


> main :: IO ()         
> main = do 
>        putStrLn "You teach me tic-tac-toe, ok ?"
>        c <- getChar
>        if c == 'n' 
>        then stop 
>        else play []

       