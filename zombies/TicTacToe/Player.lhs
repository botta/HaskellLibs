> module Player where

> import Sym

    
> data Player = PX | PO deriving (Eq, Show)

> sym :: Player -> Sym
> sym PX = X
> sym PO = O



  
