> module DynamicProgramming.ViterbiAlgorithms where

> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Ops


Likelihood of observation sequences and hidden states (HS): given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- a simple observation probability |ob|: |evalSP (ob x) y| is the
  probability of observing |y| when the HS is |x|,

- a simple initial probability |sp0|: |evalSP sp0 x| is the probability
  of |x| being the HS at step 0,

- a list of observations |ys| of length |n|, 

compute 

- the joint probability of |ys| and of |x| being the HS at step |n|.

> jointProbObsEndstate :: (Eq b) => 
>                         (a -> SimpleProb a) ->
>                         (a -> SimpleProb b) ->
>                         SimpleProb a -> 
>                         [b] -> 
>                         SimpleProb a

> jointProbObsEndstate tr ob sp0 [] 
>     = sp0

> jointProbObsEndstate tr ob sp0 (y:[]) 
>     = SP [(x, p * (evalSP (ob x) y)) | (x, p) <- xps0]
>       where xps0 = unwrapSP sp0

> jointProbObsEndstate tr ob sp0 (y:y':ys) 
>     = SP [(x, p * (evalSP (ob x) y)) | (x, p) <- xps]
>       where xps = unwrapSP sp
>             sp = jointProbObsEndstate tr ob sp0 (y':ys) >>= tr


Likelihood of observation sequences: given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- a simple observation probability |ob|: |evalSP (ob x) y| is the
  probability of observing |y| when the HS is |x|,

- a simple initial probability |sp0|: |evalSP sp0 x| is the probability
  of |x| being the HS at step 0,

- a list of observations |ys| of length |n|, 

compute 

- the probability of |ys|.

> probObs :: (Eq a, Eq b) =>
>            (a -> SimpleProb a) ->
>            (a -> SimpleProb b) ->
>            SimpleProb a -> 
>            [b] -> 
>            Prob

> probObs tr ob sp0 ys = levalSP sp (supp sp)
>     where sp = jointProbObsEndstate tr ob sp0 ys


Likelihood of hidden states (HS): given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- a simple observation probability |ob|: |evalSP (ob x) y| is the
  probability of observing |y| when the HS is |x|,

- a simple initial probability |sp0|: |evalSP sp0 x| is the probability
  of |x| being the HS at step 0,

- a list of observations |ys| of length |n|, 

compute 

- the probability of |x| being the HS at step |n|.

> probEndstate :: (Eq a, Eq b) =>
>                 (a -> SimpleProb a) ->
>                 (a -> SimpleProb b) ->
>                 SimpleProb a -> 
>                 [b] -> 
>                 SimpleProb a

> probEndstate tr ob sp0 ys = rescaleSP sp (1.0 / pobs)
>     where sp = jointProbObsEndstate tr ob sp0 ys
>           pobs = probObs tr ob sp0 ys


Likelihood of hidden state sequences given observation sequences: given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- a simple observation probability |ob|: |evalSP (ob x) y| is the
  probability of observing |y| when the HS is |x|,

- a simple initial probability |sp0|: |evalSP sp0 x| is the probability
  of |x| being the HS at step 0,

- a list of observations |ys| of length |n|, 

compute 

- for all |x|, the hidden path of length |n| ending in |x| which better
  explains |ys|.


We first implement an algorithm for solving the following auxiliary
problem: given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- an hidden state |x|,

- a simple hidden path probability |spxs|: |evalSP spxs xs| is the
  probability of the hidden path xs,

compute 

- a hidden path which can be optimally appended to |x| together with its
  probability  

> optimalAppendProb :: (Eq a) =>
>                      (a -> SimpleProb a) ->
>                      a -> 
>                      SimpleProb [a] -> 
>                      ([a], Prob)

> optimalAppendProb tr x sp = foldr (f x) ([], 0.0) (unwrapSP sp)
>     where (f x) (xs, p) (xs', p') 
>                     = if (xs == [])
>                       then (xs', p')
>                       else if (xs' == [])
>                            then (xs, p)
>                            else if ((p * evalSP (tr (head xs)) x) 
>                                     >
>                                     (p' * evalSP (tr (head xs')) x))
>                                 then (xs, p)
>                                 else (xs', p')

then we use |optimalAppendProb| to compute for all |x|, the hidden path
of length |n| ending in |x| which better explains |ys|. 

> optimalPaths :: (Eq a, Bounded a, Enum a, Eq b) =>
>                 (a -> SimpleProb a) ->
>                 (a -> SimpleProb b) ->
>                 SimpleProb a -> 
>                 [b] -> 
>                 SimpleProb [a]

> optimalPaths tr ob sp0 [] 
>     = return []

> optimalPaths tr ob sp0 (y:[]) 
>     = SP [([x], p * (evalSP (ob x) y)) | (x, p) <- xps0]
>       where xps0 = unwrapSP sp0

> optimalPaths tr ob sp0 (y:y':ys) 
>     = SP [((x:xs), p * (evalSP (ob x) y) * (evalSP (tr x') x)) | 
>           x <- listAll,
>           (xs, p) <- [optimalAppendProb tr x ops],
>           x' <- [head xs]]
>       where ops = optimalPaths tr ob sp0 (y':ys)


Likelihood of hidden state sequences given observation sequences: given 

- a simple transition probability |tr|: |evalSP (tr x) x'| is the
  probability of a transition from the HS |x| into the HS |x'|,

- a simple observation probability |ob|: |evalSP (ob x) y| is the
  probability of observing |y| when the HS is |x|,

- a simple initial probability |sp0|: |evalSP sp0 x| is the probability
  of |x| being the HS at step 0,

- a list of observations |ys| of length |n|, 

compute 

- the hidden paths of length |n| which better explain |ys| (Viterbi
  paths). 

> viterbiPaths :: (Eq a, Bounded a, Enum a, Eq b) =>
>                 (a -> SimpleProb a) ->
>                 (a -> SimpleProb b) ->
>                 SimpleProb a -> 
>                 [b] -> 
>                 [[a]]

> viterbiPaths tr ob sp0 ys = mostProbs (optimalPaths tr ob sp0 ys)


