> import DynamicProgramming.ViterbiAlgorithms
> import Prob.SimpleProb
> import Prob.SimpleProbOps


> data X            =   S | R 
>                       deriving (Eq, Show, Ord, Enum, Bounded)

> data Y            =   W | C | Sh 
>                       deriving (Eq, Show, Ord, Enum, Bounded)
 
> p0                ::  SimpleProb X
> p0                =   SP [(S, 0.4), (R, 0.6)]
 
> tr                ::  X -> SimpleProb X
> tr S              =   SP [(S, 0.6), (R, 0.4)]
> tr R              =   SP [(S, 0.3), (R, 0.7)]
 
> ob                ::  X -> SimpleProb Y
> ob S              =   SP [(W, 0.6), (C, 0.1), (Sh, 0.3)]
> ob R              =   SP [(W, 0.1), (C, 0.5), (Sh, 0.4)]


Example 1 : verify that

probObs tr ob p0 [W]
= 
levalSP sp (supp sp)
where sp = jointProbObsEndstate tr ob sp0 [W]
= 
levalSP sp (supp sp)
where sp = jointProbObsEndstate tr ob p0 (W:[])
=
levalSP sp (supp sp)
where sp = SP [(x, p * (evalSP (ob x) W)) | (x, p) <- xps0]
      xps0 = unwrapSP p0
=
levalSP sp (supp sp)
where sp = SP [(x, p * (evalSP (ob x) W)) | (x, p) <- xps0]
      xps0 = [(S, 0.4), (R, 0.6)]
=
levalSP sp (supp sp)
where sp = SP [(S, 0.4 * (evalSP (ob S) W)), 
               (R, 0.6 * (evalSP (ob R) W))]
=
levalSP sp (supp sp)
where sp = SP [(S, 0.4 * 0.6), (R, 0.6 * 0.1)]
=
levalSP sp [S, R]
where sp = SP [(S, 0.24), (R, 0.06)]
=
sum [evalSP sp x | x <- [S, R]]
where sp = SP [(S, 0.24), (R, 0.06)]
=
0.3


Example 2: verify that

levalSP sp (supp sp) == 1.0

where 

> sp = probEndstate tr ob p0 [W]


Example 3: compute

> ops = optimalPaths tr ob p0 [W]