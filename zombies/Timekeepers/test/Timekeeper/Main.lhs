> module Main where

> import Agent.Agent

> import Util.List.Ops

> import Timekeepers.Timekeeper


> t0 = makeTimekeeper 0 [1] 0
> t1 = makeTimekeeper 1 [0] 10

> ags = [t0, t1]

> ags' = iteratelAgent ags 4

> main = printList (map (showAgent "") ags')