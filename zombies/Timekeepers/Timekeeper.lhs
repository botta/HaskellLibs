> module Timekeepers.Timekeeper(makeTimekeeper) where


> import List

> import Util.List.Ops
> import Agent.Agent

> import Env


The Timekeeper's internal state

> type State = (AgentId, 
>               [(AgentId, AgentKind)], 
>               Time,
>               Bool,
>               Bool,
>               [Msg])


Constructors

> makeTimekeeper :: AgentId -> 
>                   [(AgentId, AgentKind)] -> 
>                   Time -> 
>                   Agent RR
> makeTimekeeper i iks time = Agent ident
>                                   outMsgs
>                                   step
>                                   showTimekeeper
>                                   (i, iks, time, True, True, [])

> ident :: State -> AgentId
> ident (i, iks, t, aif, tif, outs) = i

> outMsgs :: State -> [Msg] 
> outMsgs (i, iks, t, aif, tif, outs) = outs

-- > step :: State -> [Msg] -> State
-- > step s ins = s4
-- >     where s0 = s
-- >           s1 = emptyMsgs s0
-- >           s2 = if (areAllIdle s1 ins && not (wereAllIdle s1))
-- >                then incrementTime s1
-- >                else s1
-- >           s3 = foldl f s2 [(AreYouIdle, k) | k <- peers s2]
-- >           s4 = setAllIdle (areAllIdle s3 ins) s3
-- >           f s m = appendMsg m s

-- > step :: State -> [Msg] -> State
-- > step s ins = s5
-- >     where allIdle = areAllIdle s ins
-- >           s0 = s
-- >           s1 = emptyMsgs s0
-- >           s2 = if (allIdle && not (wereAllIdle s1))
-- >                then incrementTime s1
-- >                else s1
-- >           s3 = foldl f s2 [(AreYouIdle, k) | k <- peers s2]
-- >           s4 = setAllIdle allIdle s3
-- >           f s m = appendMsg m s
-- >           s5 = foldl g s4 ins
-- >           g s (AreAllIdle, k) = if allIdle
-- >                                 then appendMsg (AllIdlePositive, k) s
-- >                                 else appendMsg (AllIdleNegative, k) s
-- >           g s _ = s

> step :: State -> [Msg] -> State
> step s ins = s6
>     where allIdle = areAllIdle s ins
>           s0 = s
>           s1 = emptyMsgs s0
>           s2 = if (allIdle && not (wereAllIdle s1))
>                then incrementTime s1
>                else s1
>           s3 = setTimeIncremented (allIdle && not (wereAllIdle s1)) s2
>           s4 = foldl f s3 [(AreYouIdle, k) | k <- peers s3]
>           s5 = setAllIdle allIdle s4
>           f s m = appendMsg m s
>           s6 = foldl g s5 ins
>           g s (AreAllIdle, k) = if allIdle
>                                 then appendMsg (AllIdlePositive, k) s
>                                 else appendMsg (AllIdleNegative, k) s
>           g s _ = s


> showTimekeeper :: State -> String -> String

> showTimekeeper (i, iks, t, aif, tif, outs) ""
>     = "id:   " ++ (show i) ++ ", " ++
>       "iks:  " ++ (show iks) ++ ", " ++
>       "time: " ++ (show t) ++ ", " ++
>       "all idle flag: " ++ (show aif) ++ ", " ++
>       "time incremented flag: " ++ (show tif) ++ ", " ++
>       "outs: " ++ (show outs)

> showTimekeeper (i, iks, t, aif, tif, outs) "time"
>     = show t

> showTimekeeper (i, iks, t, aif, tif, outs) "time incremented flag"
>     = show tif

> showTimekeeper (i, iks, t, aif, tif, outs) _
>     = ""


Auxiliary functions (helpers)

Features

> identKinds :: State -> [(AgentId, AgentKind)]
> identKinds (i, iks, t, aif, tif, outs) = iks

> peers :: State -> [AgentId]
> peers s = map fst (identKinds s)


Queries

> wereAllIdle :: State -> Bool
> wereAllIdle (i, iks, t, aif, tif, outs) = aif

> areAllIdle :: State -> [Msg] -> Bool
> areAllIdle s ins = (is == js)
>     where is = sort (map snd (filter isIdlePositive ins))
>           js = sort (peers s)


Commands

> emptyMsgs :: State -> State
> emptyMsgs (i, iks, t, aif, tif, outs) = 
>     (i, iks, t, aif, tif, []) 

> appendMsg ::  Msg -> State ->  State
> appendMsg msg (i, iks, t, aif, tif, outs) =
>     (i, iks, t, aif, tif, outs ++ [msg])

> incrementTime ::  State ->  State
> incrementTime (i, iks, t, aif, tif, outs) =
>     (i, iks, t + 1, aif, tif, outs)

> setAllIdle :: Bool -> State -> State
> setAllIdle aif' (i, iks, t, aif, tif, outs) = 
>     (i, iks, t, aif', tif, outs)

> setTimeIncremented :: Bool -> State -> State
> setTimeIncremented tif' (i, iks, t, aif, tif, outs) = 
>     (i, iks, t, aif, tif', outs)

