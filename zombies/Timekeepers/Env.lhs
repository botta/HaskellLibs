> module Timekeepers.Env where

> import NumericTypes.Nat



> type Time = Nat


> type AgentId = Nat


> data AgentKind = Unknown 
>                  deriving (Eq, Show)


> data RR = AreYouIdle
>         | IdlePositive
>         | IdleNegative
>         | AreAllIdle
>         | AllIdlePositive
>         | AllIdleNegative
>           deriving (Eq, Show)


> type Msg = (RR, AgentId)


> isIdlePositive :: Msg -> Bool
> isIdlePositive (IdlePositive, k) = True
> isIdlePositive _ = False
