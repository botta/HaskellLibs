> module Basics where

> import NumericTypes.Real


> type Population = REAL

> type GrowthRate = REAL

> type EmissionRatePerCapita = REAL

> type EmissionRate = REAL

> type Emission = REAL

> type Time = REAL

> type Data country = country -> (Population, GrowthRate, EmissionRatePerCapita)

> population :: Data country ->
>               Time -> 
>               country -> 
>               Population
> population d t c = p * exp(r * t) where
>   (p, r, e) = d c

> populations :: (Enum country) => 
>                Data country -> 
>                Time -> 
>                [Population]
> populations d t = [population d t c | c <- [toEnum 0 ..]]

> totalPopulation :: (Enum country) => 
>                    Data country -> 
>                    Time -> 
>                    Population
> totalPopulation d t = sum (populations d t)

> growthRate :: Data country -> 
>               country -> 
>               GrowthRate
> growthRate d c = r where
>   (p, r, e) = d c

> growthRates :: (Enum country) => 
>                Data country -> 
>                [GrowthRate]
> growthRates d = [growthRate d c | c <- [toEnum 0 ..]]

> emissionRatePerCapita :: Data country -> 
>                          country -> 
>                          EmissionRatePerCapita
> emissionRatePerCapita d c = e where
>   (p, r, e) = d c

> emissionRatesPerCapita :: (Enum country) => 
>                           Data country -> 
>                           [EmissionRatePerCapita]
> emissionRatesPerCapita d = 
>   [emissionRatePerCapita d c | c <- [toEnum 0 ..]]

> emissionRate :: Data country -> 
>                 Time -> 
>                 country -> 
>                 EmissionRate
> emissionRate d t c = emissionRatePerCapita d c * population d t c

> emissionRates :: (Enum country) => 
>                  Data country -> 
>                  Time -> 
>                  [EmissionRate]
> emissionRates d t = 
>   [emissionRate d t c | c <- [toEnum 0 ..]]

> emission :: Data country -> 
>             Time -> 
>             country -> 
>             Emission
> emission d t c = let (p, r, e) = d c in
>                  if r == 0                                   
>                  then e * p * t
>                  else e * p * (exp(r * t) - 1) / r


> emissions :: (Enum country) => 
>              Data country -> 
>              Time -> 
>              [Emission]
> emissions d t = [emission d t c | c <- [toEnum 0 ..]]

> totalEmission :: (Enum country) => 
>                  Data country -> 
>                  Time -> 
>                  Emission
> totalEmission d t = sum (emissions d t)
  
