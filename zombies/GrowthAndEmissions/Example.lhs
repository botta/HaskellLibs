> module Example where

> import Basics
> import Assume
> import Graphics.Gnuplot.Simple
> -- import Graphics.Rendering.Chart.Simple

> data Country = EU 
>              | USA 
>              | China 
>              | India 
>              | SouthAmerica 
>              | Africa deriving (Eq, Enum, Show)

> lala :: Data Country
> lala EU           = ( 97, -0.10, 1.30)
> lala USA          = ( 12, -0.12, 1.20)
> lala China        = ( 72,  0.07, 1.10)
> lala India        = ( 32,  0.07, 1.15)
> lala SouthAmerica = (976,  0.23, 0.73)
> lala Africa       = (254,  0.17, 0.96)


> lala' :: Data Country
> lala' = assume lala (SouthAmerica, 0.0)


