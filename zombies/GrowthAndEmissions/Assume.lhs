> module Assume where

> import NumericTypes.Real
> import Basics


> assume :: (Enum country, Eq country) => 
>           Data country -> 
>           (country, GrowthRate) -> 
>           Data country
> assume d (c', r') = d' where
>   d' c = let (p, r, e) = d c in               
>          let res = [(growthRate d c'', emissionRatePerCapita d c'')   
>                     | c'' <- [toEnum 0 ..]] in                       
>           if c == c'  
>           then (p, r', interpolate res r')
>           else d c             


