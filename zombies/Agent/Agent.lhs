> module Agent.Agent(Agent(Agent), 
>                    showAgent,
>                    showlAgent,
>                    iteratelAgent,
>                    printTrj
>                   ) where


> import List
> import Maybe
> import NumericTypes.Nat


The Agent ADT

-- > data Agent m = forall s . Agent (s -> Nat)
-- >                                 (s -> [(m, Nat)]) 
-- >                                 (s -> [(m, Nat)] -> s)
-- >                                 (s -> IO ())
-- >                                 s

> data Agent m = forall s . Agent (s -> Nat) -- (Nat)
>                                 (s -> [(m, Nat)]) 
>                                 (s -> [(m, Nat)] -> s)
>                                 (s -> String -> String)
>                                 s


Auxiliary functions (core)

> ident :: Agent m -> Nat -- :: Nat
> ident (Agent i o t p s) = i s -- i

> outs :: Agent m -> [(m, Nat)] -- ParFinMap AgentId (Bag m)
> outs (Agent i o t p s) = o s

> step :: Agent m -> [(m, Nat)] -> Agent m -- Nat AgentId
> step (Agent i o t p s) ins = (Agent i o t p s')
>     where s' = t s ins

> showAgent :: String -> Agent m -> String -- Cmd -> Agent m -> Obs
> showAgent tag (Agent i o t p s) = p s tag -- showAgent cmd


Auxiliary functions (helpers)

> showlAgent :: String -> [Agent m] -> [String]
> showlAgent tag = map (showAgent tag)

Exchange messages. For the moment we do not use the D module and the
|exch| primitive. Instead, we implement an ad-hoc finction
|exchMsgs|. We will introduce distributed data when it becomes clear
what it makes most sense to distribute (single agents, groups of agents,
etc.) in a SPMD model.

> inMsgs :: [Agent m] -> (Nat -> [(m, Nat)])
> inMsgs ags = \i -> outs (fromJust (find ((i ==) . ident) ags))

> outMsgs :: [Agent m] -> (Nat -> [(m, Nat)])
> outMsgs ags = \i -> [(rr, i') | i' <- map ident ags,
>                                 (rr, i'') <- (inMsgs ags) i',
>                                 i'' == i]

|inMsgs| and |outMsgs| have to fulfill the following specification:

(rr, j) elem (outMsgs ags i) <=> (rr, i) elem (inMsgs ags j)

> exchMsgs :: [Agent m] -> [[(m, Nat)]]
> exchMsgs ags = [outMsgs ags i | i <- map ident ags]


Iterate a list of agents

> iteratelAgent :: [Agent m] -> Nat -> [Agent m]
> iteratelAgent ags 0 = ags
> iteratelAgent ags (n + 1) = iteratelAgent ags' n
>   where ags' = [step ag msgs | (ag, msgs) <- zip ags msgss]
>         msgss = exchMsgs ags

Print trajectory

> printTrj :: ([Agent m] -> Bool) ->
>             ([Agent m] -> String) ->
>             [Agent m] -> 
>             Nat -> 
>             IO [Agent m]
> printTrj pred showl ags 0 = 
>     do print ("iter: " ++ (show 0) ++ "; " ++ showl ags)
>        return ags
> printTrj pred showl ags (n + 1) = 
>     do ags' <- printTrj pred showl ags n
>        ags'' <- return (iteratelAgent ags' 1)
>        f ags''
>            where -- f :: [Agent m] -> IO [Agent m]
>                  f agents = if (pred agents)
>                             then do print ("iter: " ++ (show (n + 1)) ++ "; " ++ showl agents)  
>                                     return agents
>                             else return agents


