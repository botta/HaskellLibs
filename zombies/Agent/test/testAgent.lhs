> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Random.Ops
> import Util.Unsafe.Random.Ops
> import NumericTypes.Nat
> import Agent.Agent


Common producer / consumer

> type Stock = Double
> type Msg = (Double, Nat)


Producer State

> type ExpectedDemand = Double
> type ExpectedProductionFluctuation = Double
> type ProducerState = (Nat, 
>                       [Nat], 
>                       Stock, 
>                       Nat,
>                       ExpectedDemand, 
>                       ExpectedProductionFluctuation,
>                       SimpleProb Double)


Producer constructor

> makeProducer :: Nat -> 
>                 [Nat] -> 
>                 Stock -> 
>                 SimpleProb Double ->
>                 Agent Double
> makeProducer i is stock productionFluctuationSP
>     = Agent identProducer
>             outMsgsProducer
>             stepProducer
>             printProducer
>             (i, 
>              is, 
>              stock, 
>              expectedNumberOfConsumers,
>              expectedDemand,
>              expectedProductionFluctuation,
>              productionFluctuationSP)
>       where expectedNumberOfConsumers = max 1 (div (length is) 2)
>             expectedDemand = stock
>             expectedProductionFluctuation = unsafeRandomize productionFluctuationSP


Producer auxiliary functions (core)

> identProducer :: ProducerState -> Nat
> identProducer (i, 
>                is, 
>                stock, 
>                expectedNumberOfConsumers, 
>                expectedDemand, 
>                expectedProductionFluctuation, 
>                productionFluctuationSP) = i

> outMsgsProducer :: ProducerState -> [Msg]
> outMsgsProducer (i, 
>                  is, 
>                  stock, 
>                  expectedNumberOfConsumers, 
>                  expectedDemand, 
>                  expectedProductionFluctuation, 
>                  productionFluctuationSP)
>     = [(deliver, i) | i <- is]
>       where deliver = - (min stock expectedDemand) / (realToFrac n)
>             n = expectedNumberOfConsumers

> stepProducer :: ProducerState -> [Msg] -> ProducerState
> stepProducer (i, 
>               is, 
>               stock, 
>               expectedNumberOfConsumers, 
>               expectedDemand, 
>               expectedProductionFluctuation, 
>               productionFluctuationSP) ins 
>     = (i, 
>        is, 
>        stock',
>        expectedNumberOfConsumers', 
>        expectedDemand', 
>        expectedProductionFluctuation', 
>        productionFluctuationSP)
>       where stock' 
>                 = stock + production + productionFluctuation - deliver
>             production 
>                 = max 0 (expectedDemand - (stock + expectedProductionFluctuation - deliver))
>             deliver = min stock expectedDemand
>             expectedNumberOfConsumers' = max 1 (div (expectedNumberOfConsumers + numberOfConsumers) 2)
>             expectedDemand' = 0.5 * (expectedDemand + demand)
>             expectedProductionFluctuation' 
>                 = 0.5 * (expectedProductionFluctuation + productionFluctuation)
>             numberOfConsumers = length (filter ((> 0.0) . fst) ins)
>             demand = sum (map ((max 0.0) . fst) ins)
>             productionFluctuation = unsafeRandomize productionFluctuationSP

> printProducer :: ProducerState -> IO () 
> printProducer (i, 
>                is, 
>                stock, 
>                expectedNumberOfConsumers, 
>                expectedDemand, 
>                expectedProductionFluctuation, 
>                productionFluctuationSP)
>     = do print ("id:                            " ++ (show i))
>          print ("is:                            " ++ (show is))
>          print ("stock:                         " ++ (show stock))
>          print ("expectedNumberOfConsumers:     " ++ (show expectedNumberOfConsumers))
>          print ("expectedDemand:                " ++ (show expectedDemand))
>          print ("expectedProductionFluctuation: " ++ (show expectedProductionFluctuation))





Consumer State

> type ExpectedDeliver = Double
> type ExpectedConsume = Double
> type ConsumerState = (Nat, 
>                       [Nat],
>                       Stock,
>                       Nat,
>                       ExpectedDeliver, 
>                       ExpectedConsume,
>                       SimpleProb Double)


Consumer constructor

> makeConsumer :: Nat -> 
>                 [Nat] ->
>                 Stock ->
>                 SimpleProb Double ->
>                 Agent Double
> makeConsumer i is stock consumeSP 
>     = Agent identConsumer
>             outMsgsConsumer
>             stepConsumer
>             printConsumer
>             (i, 
>              is, 
>              stock, 
>              expectedNumberOfProducers,
>              expectedDeliver,
>              expectedConsume,
>              consumeSP)
>       where expectedNumberOfProducers = max 1 (div (length is) 2)
>             expectedDeliver = expectedConsume
>             expectedConsume = unsafeRandomize consumeSP


Consumer auxiliary functions (core)

> identConsumer :: ConsumerState -> Nat
> identConsumer (i, 
>                is, 
>                stock, 
>                expectedNumberOfProducers, 
>                expectedDeliver, 
>                expectedConsume, 
>                consumeSP) = i

> outMsgsConsumer :: ConsumerState -> [Msg] 
> outMsgsConsumer (i, 
>                  is, 
>                  stock,
>                  expectedNumberOfProducers,
>                  expectedDeliver, 
>                  expectedConsume,
>                  consumeSP) 
>     = [(demand, i) | i <- is]
>       where demand = (max 0.0 (stockTarget - expectedStock)) / (realToFrac n)
>             expectedStock = stock + expectedDeliver - expectedConsume
>             stockTarget = 10.0
>             n = expectedNumberOfProducers

> stepConsumer :: ConsumerState -> [Msg] -> ConsumerState
> stepConsumer (i, 
>               is, 
>               stock, 
>               expectedNumberOfProducers, 
>               expectedDeliver, 
>               expectedConsume, 
>               consumeSP) ins 
>     = (i, 
>        is, 
>        stock', 
>        expectedNumberOfProducers', 
>        expectedDeliver', 
>        expectedConsume',
>        consumeSP)
>     where stock' = stock + deliver - consume
>           expectedNumberOfProducers' = max 1 (div (expectedNumberOfProducers + numberOfProducers) 2)
>           expectedDeliver' = 0.5 * (expectedDeliver + deliver)
>           expectedConsume' = 0.5 * (expectedConsume + consume)
>           numberOfProducers = length (filter ((< 0.0) . fst) ins)
>           deliver = - sum (map ((min 0.0) . fst) ins)
>           consume = min (unsafeRandomize consumeSP) (stock + deliver)

> printConsumer :: ConsumerState -> IO () 
> printConsumer (i, 
>                is, 
>                stock, 
>                expectedNumberOfProducers, 
>                expectedDeliver, 
>                expectedConsume, 
>                consumeSP)
>     = do print ("id:                        " ++ (show i))
>          print ("is:                        " ++ (show is))
>          print ("stock:                     " ++ (show stock))
>          print ("expectedNumberOfProducers: " ++ (show expectedNumberOfProducers))
>          print ("expectedDeliver:           " ++ (show expectedDeliver))
>          print ("expectedConsume:           " ++ (show expectedConsume))




> producerStock = 100.0
> productionFluctuationSP = makeUniformSP [0.0..0.0]
> producer = makeProducer 0 [1] producerStock productionFluctuationSP

> consumerStock = 10.0
> consumeSP = makeUniformSP [0.0..10.0]
> consumer = makeConsumer 1 [0] consumerStock consumeSP

> ags = [producer, consumer]

> ags' = iteratelAgent ags 1