> module Util.List.List() where



> import Data.List
> import Util.List.Listify
> import Util.Set.SetEq
> import Util.Set.SetOrd
> import Util.Set.SetOps
> import Util.Set.GeneralizedSetOps


-- > import Util.SetOps.SetOps



> instance (Eq a) => Listify [] a where
>   listify = id


> instance (Eq a) => SetEq ([] a) where
>   s_eq as as' = (length nas == length nas') && (nas \\ nas' == [])
>     where nas = nub as
>           nas' = nub as'


> instance (Eq a) => SetOrd ([] a) where
>   s_subeq as as' = ((nub as) \\ as' == [])


> instance (Eq a) => SetOps ([] a) where
>   s_empty = []
>   s_join as as' = union as as'
>   s_subtract as as' = as \\ as'


-- > instance (Eq a) => SetOps [] a where
-- >   intersection as as' = intersect as as'
-- >   union as as' = List.union as as'
-- >   difference as as' = as \\ as'
-- >   empty = []
-- >   is_empty [] = True
-- >   is_empty (_:_) = False
-- >   is_singleton as = length (nub as) == 1
-- >   is_in = elem


