> module Util.List.Ops where

> import List


> normalize :: (Eq a, Num b) => [(a, b)] -> [(a, b)]
> normalize abs = [(a, f a) | a <- nub (map fst abs)]
>     where f a = sum [b' | (a', b') <- abs, a' == a]

> eval :: (Eq a, Num b) => [(a, b)] -> a -> b
> eval abs a = sum [b' | (a', b') <- abs, a' == a]

> dot :: (Eq a, Num b) => [(a, b)] -> [(a, b)] -> b
> dot abs abs' = sum [b * (eval abs' a) | (a, b) <- abs]

dot should commute (on infinite precision numbers): 

  dot abs abs' == dot abs' abs

> rescale :: (Num b) => [(a, b)] -> b -> [(a, b)]
> rescale abs t = [(a, b * t) | (a, b) <- abs]

> areAllPositive :: (Ord a, Num a) => [a] -> Bool
> areAllPositive xs = (filter (<= 0) xs) == []



IO

> printList :: (Show a) => [a] -> IO ()
> printList [] = return ()
> printList (x:xs) = do print x
>                       putStrLn ""
>                       printList xs

> printLists :: (Show a) => [[a]] -> IO ()
> printLists [] = return ()
> printLists (x:xs) = do printList x
>                        putStrLn ""
>                        printLists xs





