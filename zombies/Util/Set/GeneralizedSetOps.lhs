> module Util.Set.GeneralizedSetOps where



> import Util.Set.SetOps
> import Util.List.Listify



> class (SetOps a) => GeneralizedSetOps c a where
>   gs_intersect :: c a -> a
>   gs_join :: c a -> a


> instance (Listify c a, SetOps a) => GeneralizedSetOps c a where
>   gs_join cx = f (listify cx) where
>     f [] = s_empty  
>     f (x:[]) = x
>     f (x:xs) = s_join x (f xs)


