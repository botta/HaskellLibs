> module Util.Set.SetEq where



> class SetEq a where
>   s_eq, s_neq :: a -> a -> Bool
>
>   -- Minimal complete definition: s_eq or s_neq
>   x `s_eq` y = not (x `s_neq` y)
>   x `s_neq` y = not (x `s_eq` y)
