> module Util.Set.SetOrd where



> import Util.Set.SetEq



> class (SetEq a) => SetOrd a where
>   s_compare :: a -> a -> Ordering
>   s_sub, s_subeq, s_supeq, s_sup :: a -> a -> Bool
>
>   -- Minimal complete definition: s_subeq or compare
>   -- Using compare can be more efficient for complex types.
>   s_compare x y | s_eq x y    = EQ
>                 | s_subeq x y =  LT
>                 | otherwise   =  GT
>   s_subeq x y = s_compare x y /= GT
>   s_sub   x y = s_compare x y == LT
>   s_supeq x y = s_compare x y /= LT
>   s_sup   x y = s_compare x y == GT
