> module Util.Set.SetOps where



> class SetOps a where
>   s_empty :: a
>   s_intersect, s_join, s_subtract :: a -> a -> a
>
>   s_intersect x y = s_subtract x (s_subtract x y)



Axioms:

1) e `s_eq` s_empty => 
     - (s_intersect x e) `s_eq` e 
     - (s_join x e) `s_eq` x                       

2) Idempotence: 
     - (s_intersect x x) `s_eq` x 
     - (s_join x x) `s_eq` x                       
                      
3) Commutativity: 
     - (s_intersect x y) `s_eq` (s_intersect y x)
     - (s_join x y) `s_eq` (s_join y x)                      
                      
4) Associativity:


5) Distributivity:
