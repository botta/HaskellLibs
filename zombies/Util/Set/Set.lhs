> module Util.Set.Set(Set, 
>                     Setify, 
>                     setify) where


> import List(nub,sort)
> import Util.List.List
> import Util.SetOps.SetOps



> data Set a = Set [a]



> class Setify c a where
>   setify :: c a -> Set a


> instance (Eq a) => Setify [] a where
>   setify as = Set (nub as)


> instance (Eq a, SetOps [] a) => Eq (Set a) where
>   Set as == Set as' = difference as as' == difference as' as


> instance (Ord a, Show a) => Show (Set a) where
>   show (Set as) = show (sort (nub as))


> instance (Eq a, SetOps [] a) => SetOps Set a where
>   intersection (Set as) (Set as') = setify (intersection as as')
>   union (Set as) (Set as') = setify (union as as')
>   difference (Set as) (Set as') = setify (difference as as')
>   empty = Set []
>   is_empty (Set as) = is_empty as
>   is_singleton (Set as) = is_singleton as

