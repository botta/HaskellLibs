> module Util.Random.Ops where

> import List
> import Maybe
> import Random

--> import Numeric

> import Prob.SimpleProb
> import Prob.SimpleProbOps


-- > randomize :: SimpleProb a -> IO a
-- > randomize (SP ds) = do x <- randomRIO (0.0, 1.0)
-- >                        xs <- return (map sum (tail (inits (map snd ds))))
-- >                        k <- return (fromJust (findIndex (> x) xs))
-- >                        return (fst (ds!!k))

> randomize :: SimpleProb a -> IO a
> randomize (SP xps) = do spx <- return (SP xps)
>                         spispx <- return (makeInitsSP spx)
>                         ixps <- return (unwrapSP spispx)
>                         ips <- return (map snd (tail ixps))
>                         p <- randomRIO (0.0, 1.0)
>                         k <- return (fromJust (findIndex (> p) ips))
>                         return (fst (xps!!k))
