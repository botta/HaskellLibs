> module Util.Tree.Tree where


> data Tree a = Leaf a | Twig [Tree a]

> flattenTree :: Tree a -> [a]
> flattenTree (Leaf a) = [a]
> flattenTree (Twig tas) = concat (map flattenTree tas)



