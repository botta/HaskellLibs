> module Util.Unsafe.Random.Ops(unsafeRandomize,
>                               unsafeRandomPermute) where

> import List

> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Random.Ops
> import Util.Unsafe.Ops

> unsafeRandomize :: SimpleProb a -> a
> unsafeRandomize = unsafePerformIO . randomize

> unsafeRandomPermute :: (Eq a) => [a] -> [a] 
> unsafeRandomPermute xs = f [] xs 
>     where f :: (Eq a) => [a] -> [a] -> [a]
>           f ys [] = ys
>           f ys zs = f (z:ys) (delete z zs)
>               where z = unsafeRandomize (makeUniformSP zs)
