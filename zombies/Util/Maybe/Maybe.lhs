> module Util.Maybe.Maybe where



> flipListMaybe :: [Maybe a] -> Maybe [a]
> flipListMaybe mas = foldl f (Just []) mas where
>   f Nothing _ = Nothing  
>   f (Just as) Nothing = Nothing  
>   f (Just as) (Just a) = Just (as ++ [a])