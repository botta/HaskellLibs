Useful functions, surprisingly absent from the standard prelude.

> module Util.Ops where

> pair                          ::  (a -> b, a -> c) -> a -> (b, c)
> pair (f, g) x                 =   (f x, g x)

> cross                         ::  (a -> b, c -> d) -> (a, c) -> (b, d)
> cross (f, g)                  =   pair (f . fst, g . snd)

> cond                          ::  (a -> Bool) -> (a -> b, a -> b)
>                                               -> a -> b
> cond p (f, g) a               =   if p a then f a else g a

> (<>)                          ::  Monad m => (b -> m c) 
>                                           -> (a -> m b)
>                                           -> (a -> m c)  
> g <> f                        =   \ a -> (f a) >>= g

> lbind                         ::  Monad m => (a -> m b, m a)
>                                           -> m b
> lbind                         =   uncurry (=<<)

> rbind                         ::  Monad m => (m a, a -> m b) -> m b
> rbind                         =   uncurry (>>=)

> listAll                       ::  (Enum a, Bounded a) => [a]
> listAll                       =   [minBound .. maxBound]

========================================================================
-- Instance declarations
========================================================================

> instance (Show a, Enum a, Bounded a, Show b) => Show (a -> b) where
>   show f                      =   show (map (pair(id, f)) listAll)

> instance (Enum a, Bounded a, Eq b) => Eq (a -> b) where
>   f == g                      =   and (map p listAll)
>                                   where
>                                   p a = f a == g a
