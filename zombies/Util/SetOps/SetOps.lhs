> module Util.SetOps.SetOps(
>   SetOps, 
>   intersection,
>   union,
>   difference,
>   empty,
>   is_empty,
>   is_singleton,
>   is_in,
>   subeq,
>   eq,
>   are_disjoint,
>   are_pairwise_disjoint,
>   c_intersection,
>   c_union) where



> import List (nub)
> import Util.List.Listify



> class SetOps c a where

>   empty :: c a

>   intersection :: c a -> c a -> c a

>   union :: c a -> c a -> c a

>   difference :: c a -> c a -> c a

>   is_empty :: c a -> Bool

>   is_singleton :: c a -> Bool

>
>   is_in :: a -> c a -> Bool
>     -- is_in a empty = False
>
>   subeq :: c a -> c a -> Bool
>   a `subeq` b = is_empty (difference a b)
>
>   eq :: c a -> c a -> Bool
>   a `eq` b = a `subeq` b && b `subeq` a
>
>   are_disjoint :: c a -> c a -> Bool
>   are_disjoint a b = is_empty (intersection a b)
>
>   are_pairwise_disjoint :: (Eq a, Listify c' (c a), Listify c a) => c' (c a) -> Bool
>   are_pairwise_disjoint cas = as == nub as where 
>     as = concat (map listify (listify cas))
>
>   c_intersection :: (SetOps c a, Listify c' (c a)) => c' (c a) -> c a
>   c_intersection cas = f (listify cas) where
>     f [] = error "Util.SetOps.SetOps: c_intersection: empty argument."
>     f (as:[]) = as  
>     f (as:ass) = intersection as (f ass)
>
>   c_union :: (SetOps c a, Listify c' (c a)) => c' (c a) -> c a
>   c_union cas = f (listify cas) where
>     f [] = error "Util.SetOps.SetOps: c_union: empty argument."
>     f (as:[]) = as  
>     f (as:ass) = union as (f ass)


Axioms:

unit: 
  intersection s empty `eq` intersection empty s `eq` empty
  union s empty `eq` union empty s `eq` s
  difference s empty `eq` s
  difference empty s `eq` empty
             
idempotence: 
  intersection s s = s
  union s s = s

commutativity:


associativity:

  is_empty empty = True
  is_singleton empty = False



-- > instance (SetOps c a) => Eq (c a) where
-- >   a == b = is_subeq a b && is_subeq b a

