> module EconomicAgents.Schedule(Schedule,
>                                makeSchedule) where


> import EconomicAgents.Action


> type Period = Int


> type Schedule = [[Action]]


> makeSchedule :: [(Action, Period)] -> Schedule
> makeSchedule [] = [[Idle]]
> makeSchedule aps = (map snd (foldl f e aps)) ++ [[Idle]]
>     where pmax = maximum (map snd aps)
>           e = [(n, []) | n <- [1..pmax]]
>           f nlas (a, p) = map (g a p) nlas
>           g a p (n, la) = if (mod n p == 0)
>                           then (n, la ++ [a])
>                           else (n, la)

