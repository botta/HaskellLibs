> module EconomicAgents.ConsumerOps where


> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Unsafe.Random.Ops
> import Env


> unsafeRandomizePrices :: Prob -> 
>                          SimpleProb Price -> 
>                          [(Good, Price)]-> 
>                          [(Good, Price)]

> unsafeRandomizePrices mutRate spp gps 
>     = [(g, unsafeRandomize (mutProb p)) | (g, p) <- gps]
>       where mutProb :: Price -> SimpleProb Price
>             mutProb p = makeConvCombSP mutRate spp (makeConcentratedSP p)


-- > randomizePrices :: [(Good, Price)]-> [(Good, Price)]
-- > randomizePrices gps = gps1
-- >  where gps0 = gps
-- >	 sp = toUniformSP [1.0 .. 100.0]
-- >	 toMutSP p = convexCombination mutRate sp (SP [(p,1)])
-- > 	 gps1 = [ (g, unsafeRandomize (toMutSP p))) | (g, p) <- gps0 ]


-- > shuffleList :: Eq a => [a] -> IO [a] 
-- > shuffleList xs = if null xs 
-- >                     then return xs
-- >                     else
-- >                         do 
-- >       	      	    x  <- randomize (toUniformSP xs)
-- >                           xs' <- shuffleList (delete x xs)
-- >                           return (x:xs')


