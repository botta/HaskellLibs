#!/usr/bin/runhugs -98

> module Main where

> import EconomicAgents.ScarfOps


> main = print (scarfUtility [(0, 1.0),(1, 3.2)] [(0, 1.0),(1, 3.2)])

assert (scarfUtility [(0, 1.0),(1, 3.2)] [(0, 1.0),(1, 3.2)]) == 1.0