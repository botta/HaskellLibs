#!/usr/bin/runhugs -98

> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import Util.List.Ops

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters

> nScarfConsumer :: Nat
> nScarfConsumer = 100

> nIter :: Nat
> nIter = 2


Setup population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(ScarfConsumer 0 [], nScarfConsumer)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupEmptyNet agentKinds


Construct agents

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, ScarfConsumer g sgqs) iks
>     = makeScarfConsumer i iks g sgqs gps [] sch
>       where gps = [(0,1.0),(1,2.0)]
>             sch = makeSchedule [(Mutate, 1), (Idle, nIter + 1)]

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme

> ags' = iteratelAgent ags nIter

> main = printList (map (showAgent "") ags')

