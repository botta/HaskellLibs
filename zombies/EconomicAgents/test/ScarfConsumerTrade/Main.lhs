#!/usr/bin/runhugs -98

> module Main where

> import Agent.Agent

> import NumericTypes.Nat
> import Util.List.Ops

> import EconomicAgents.ScarfConsumer
> import EconomicAgents.Action
> import EconomicAgents.Schedule

> import AgentSimulation.SimulationSetup

> import Env



Convenience types

> type AgentPopulation = [(AgentKind, Nat)]
> type AgentsKind = [AgentKind]
> type AgentNetwork = [(AgentId, AgentKind)]
> type AgentsNetwork = [AgentNetwork]
> type Agents = [Agent RR]


Global parameters

> nScarfConsumer :: Nat
> nScarfConsumer = 2

> nIter :: Nat
> nIter = 10


Setup population, kinds, network 

> agentPopulation :: AgentPopulation
> agentPopulation = [(ScarfConsumer 0 [(0, 5.0), (1, 10.0)], 1), 
>                    (ScarfConsumer 1 [(0, 5.0), (1, 10.0)], 1)]

> agentKinds :: AgentsKind
> agentKinds = plainSetupKinds agentPopulation

> agentNetwork :: AgentsNetwork
> agentNetwork = setupAllToAllOthersNet agentKinds


Construct agents

> agentConstructionScheme :: (AgentId, AgentKind) ->
>                            AgentNetwork ->
>                            Agent RR

> agentConstructionScheme (i, ScarfConsumer 0 sgqs) iks
>     = makeScarfConsumer i iks 0 sgqs gps [(0, 1.0)] sch
>     where gps = [(0, 1.0), (1, 2.0)]

> agentConstructionScheme (i, ScarfConsumer 1 sgqs) iks
>     = makeScarfConsumer i iks 1 sgqs gps [(1, 1.0)] sch
>     where gps = [(0, 1.0), (1, 2.0)]

> sch :: Schedule
> sch = [[Trade], [Idle]]

> ags :: Agents
> ags = setupAgents 
>       agentKinds 
>       agentNetwork 
>       agentConstructionScheme


Iterate agents

> trj :: a -> (a -> a) -> Nat -> [a]
> trj a f 0 = [a]
> trj a f (n + 1) = ((f a'):as')
>     where a' = head as'
>           as' = trj a f n 

> agss = trj ags (flip iteratelAgent 1) nIter

> main = printLists (map (showlAgent "") agss)



Post processing functions

> goodsAndQuantities :: Agents -> [(Good, Quantity)]
> goodsAndQuantities ags = map head pss
>     where pss = map read (showlAgent "goodsAndQuantities" ags)

