> module EconomicAgents.ScarfConsumer(makeScarfConsumer ,
>  	 			      makeSomeGoodsScarfConsumer) where


> import List
> import Maybe

> import NumericTypes.Nat
> import NumericTypes.Real
> import Agent.Agent
> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Random.Ops
> import Util.Unsafe.Random.Ops
> import Util.List.Ops

> import EconomicAgents.ScarfOps
> import EconomicAgents.ConsumerOps
> import EconomicAgents.Action
> import EconomicAgents.Schedule
> import EconomicAgents.OfferFunctions

> import Env


The ScarfConsumer's internal state

> type InvariantCharacteristics = (AgentKind, 
>                                  [(Good, Quantity)], 
>                                  [(Good, Quantity)] -> REAL,
>                                  (REAL, SimpleProb Price),
>                                  Schedule)
 
> type State = (AgentId, 
>               InvariantCharacteristics,
>               [(AgentId, AgentKind)], 
>               [(Good, Price)], 
>               [(Good, Quantity)],					
>               Utility, 
>               Schedule, 
>               [Msg])


Constructors

Rem: standard schedule is [[Produce], 
                           [Trade], [Trade], [Trade], ... 
                           [Consume], 
                           [Learn], 
                           [Idle]] 

> makeScarfConsumer :: AgentId -> 
>                      [(AgentId, AgentKind)] -> 
>                      Good->
>                      [(Good, Quantity)]-> 
>                      [(Good, Price)] -> 
>                      [(Good, Quantity)]-> 
>                      (REAL, SimpleProb Price) ->
>                      Schedule -> 
>                      Agent RR

> makeScarfConsumer i iks g sgqs gps gqs mps sch 
>     = Agent ident
>             outMsgs
>             step
>             showScarfConsumer
>             (i, inv, iks, gps, gqs, 0, sch, [])
>     where inv = (k, prod, uf, mps, sch)
>	    k = ScarfConsumer g sgqs
>	    prod = [(g, eval sgqs g)]
>	    uf = scarfUtility sgqs


> makeSomeGoodsScarfConsumer :: AgentId -> 
>                      [(AgentId, AgentKind)] -> 
>                      Good->
>                      [(Good, Quantity)]-> 
>                      [(Good, Price)] -> 
>                      [(Good, Quantity)]->
>                      (REAL, SimpleProb Price) ->
>                      Schedule -> 
>                      Agent RR

> makeSomeGoodsScarfConsumer i iks g sgqs gps gqs mps sch 
>     = Agent ident
>             outMsgs
>             step
>             showScarfConsumer
>             (i, inv, iks, gps, gqs, 0, sch, [])
>     where inv = (k, prod, uf, mps, sch)
>	    k = SomeGoodsScarfConsumer g sgqs
>	    prod = [(g, eval sgqs g)]
>	    uf = scarfUtility sgqs


Auxiliary functions (core)

> ident :: State -> AgentId
> ident (i, inv, iks, gps, gqs, u, sch, outs) = i

> outMsgs :: State -> [Msg] 
> outMsgs (i, inv, iks, gps, gqs, u, sch, outs) = outs

> step :: State -> [Msg] -> State
> step s ins = foldr f s' actions
>     where actions = headSchedule s
>           s' = emptyMsgs s
>	    f Trade = tradeAction ins
>	    f Communicate = communicateAction ins
>           f Produce = produceAction ins
>           f Consume = consumeAction ins
>           f Learn = learnAction ins
>	    f Mutate = mutateAction ins
>           f Idle = idleAction ins


> showScarfConsumer :: State -> String -> String

> showScarfConsumer (i, inv, iks, gps, gqs, u, sch, outs) "" 
>     = "id: " ++ (show i) ++ ", " ++
>       "iks: " ++ (show iks) ++ ", " ++
>       "gps: " ++ (show gps) ++ ", " ++
>       "gqs: " ++ (show (normalize gqs)) ++ ", " ++
>       "u: " ++ (show u) ++ ", " ++
>       "sch: " ++ (show sch) ++ ", " ++
>       "outs: " ++ (show outs)

> showScarfConsumer (i, inv, iks, gps, gqs, u, sch, outs) "prices" 
>     = show (map snd gps)

> showScarfConsumer (i, inv, iks, gps, gqs, u, sch, outs) "utility" 
>     = show u

> showScarfConsumer (i, inv, iks, gps, gqs, u, sch, outs) "goodsAndQuantities" 
>     = "gqs: " ++ (show (normalize gqs))

> showScarfConsumer (i, inv, iks, gps, gqs, u, sch, outs) "outs" 
>     = (show outs)

> showScarfConsumer s "scarfConsumerIds" 
>     = show (scarfConsumerIds s)




Auxiliary functions (helpers)

Features

> identKinds :: State -> [(AgentId, AgentKind)]
> identKinds (i, inv, iks, gps, gqs, u, sch, outs) = iks

> goodPrices :: State -> [(Good, Price)]
> goodPrices (i, inv, iks, gps, gqs, u, sch, outs) = gps

> goodQuantities :: State -> [(Good, Quantity)]
> goodQuantities (i, inv, iks, gps, gqs, u, sch, outs) = gqs

> utility :: State -> REAL
> utility (i, inv, iks, gps, gqs, u, sch, outs) = u

> kind :: State -> AgentKind
> kind (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) = k

> utilityWeights :: State -> [(Good, Quantity)]
> utilityWeights (i, 
>                 (SomeGoodsScarfConsumer g sgqs, pgqs, uf, mps, isch), 
>                 iks, 
>                 gps, 
>                 gqs, 
>                 u, 
>                 sch, 
>                 outs) = sgqs

> mutRate :: State -> REAL
> mutRate (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) 
>     = fst mps

> mutPricesDist :: State -> SimpleProb Price
> mutPricesDist (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) 
>     = snd mps

> internalSchedule :: State -> Schedule
> internalSchedule (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) = isch

> goodProduced :: State -> Good 
> goodProduced s = f (kind s)
> 	  where f (ScarfConsumer g sgqs)  =g  
>	  	f (SomeGoodsScarfConsumer g sgqs) =g  

> production :: State -> [(Good,Quantity)]
> production (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) = pgqs

> utilityFunction :: State -> ([(Good, Quantity)] -> REAL)
> utilityFunction (i, (k, pgqs, uf, mps, isch) , iks, gps, gqs, u, sch, outs) 
>     = uf 

> peers :: State -> [AgentId]
> peers s = map fst (identKinds s)

> kinds :: State -> [AgentKind]
> kinds s = map snd (identKinds s)

> timekeeperId :: State -> AgentId
> timekeeperId s = fst (fromJust (find pred (identKinds s))) 
>    where pred a = (Timekeeper == snd a)           

> scarfConsumerIds :: State -> [AgentId]
> scarfConsumerIds s = map fst (filter pred (identKinds s))
>     where pred (i, ScarfConsumer g sgqs) = True
>     	    pred (i,SomeGoodsScarfConsumer g sgqs) = True
>           pred (i, _) = False

> otherOwnKindIds :: State -> [AgentId]
> otherOwnKindIds s = map fst (filter pred (identKinds s))
>     where pred (i,k) = (k== kind s) &&  not (i == (ownId s) )

 

> ownId :: State -> AgentId
> ownId (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) = i





Queries


> isThereTimekeeper :: State -> Bool
> isThereTimekeeper s = elem Timekeeper (kinds s)


Commands

> emptyMsgs :: State -> State
> emptyMsgs (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs, u, sch, []) 

> appendMsg ::  Msg -> State ->  State
> appendMsg msg (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs, u, sch, outs ++ [msg])

> setGoodPrices :: [(Good, Price)] -> State -> State
> setGoodPrices gps' (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps', gqs, u, sch, outs) 

> setUtility :: Utility -> State -> State
> setUtility u' (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs, u', sch, outs) 

> setGoodQuantities :: [(Good, Quantity)] ->  State -> State
> setGoodQuantities gqs' (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs', u, sch, outs) 

> appendGoodQuantities :: [(Good,Quantity)] ->  State ->  State
> appendGoodQuantities gqs' (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs ++ gqs', u, sch, outs)

> headSchedule :: State ->  [Action]
> headSchedule (i, inv, iks, gps, gqs, u, sch, outs) = head sch

> resetSchedule :: State -> State
> resetSchedule (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, sch, outs) =
>     (i, (k, pgqs, uf, mps, isch), iks, gps, gqs, u, isch, outs)

> popSchedule :: State ->  ([Action], State)
> popSchedule (i, inv, iks, gps, gqs, u, sch, outs) =
>     (head sch, (i, inv, iks, gps, gqs, u, tail sch, outs))

> pushSchedule :: [Action] -> State -> State
> pushSchedule actions (i, inv, iks, gps, gqs, u, sch, outs) =
>     (i, inv, iks, gps, gqs, u, (actions:sch), outs)

> unschedule :: [Action] -> State -> State
> unschedule actions (i, inv, iks, gps, gqs, u, (acts:actss), outs) =
>      (i, inv, iks, gps, gqs, u, sch', outs)
>    where sch' = if acts' == []
>                 then actss
>                 else (acts':actss)
>          acts' = filter (not . ((flip elem) actions)) acts


Actions     
           
> idleAction :: [Msg] -> State -> State
> idleAction ins s = s2
> --- requires (filter isAccept ins == [])
>     where s0 = s
>           s1 = foldl f s0 ins
>	    f s (Observe,k) = appendMsg (Reveal (utility s) (goodPrices s), k ) s
>           f s (Offer gqss, k) = appendMsg (Accept [], k) s
>           f s (AreYouIdle, k) = appendMsg (IdlePositive, k) s
>           f s (AllIdlePositive, k) = resetSchedule s
>           f s (_, _) = s 
>           s2 = if (isThereTimekeeper s1)
>                then appendMsg (AreAllIdle, (timekeeperId s1)) s1
>                else s1

                   
> produceAction :: [Msg] -> State -> State
> produceAction ins s = s2
>     where s0 = s
>           s1 = appendGoodQuantities (production s0) s0
>           s2 = unschedule [Produce] s1


> consumeAction :: [Msg] -> State -> State
> consumeAction ins s = 
>     case (kind s) of 
>     ScarfConsumer g sgqs 
>         -> s3
>            where s0 = s
>                  gqs = goodQuantities s0  
>                  uf = utilityFunction s0
>                  s1 = setUtility (uf gqs) s0
>                  s2 = setGoodQuantities [] s1
>                  s3 = unschedule [Consume] s2
>     SomeGoodsScarfConsumer g sgqs
>         -> s3'
>            where s0 = s
>                  gqs = goodQuantities s0
>                  cgqs = scarfSomeGoodsConsumption g sgqs gqs
>                  uf = utilityFunction s0
>                  s1 = setUtility ((utility s0) + (uf cgqs)) s0
>                  s2 = appendGoodQuantities (rescale cgqs (- 1.0)) s1 
>                  s3 = unschedule [Consume] s2
>                  s3' = s3



> tradeAction :: [Msg] -> State -> State
> tradeAction ins s = s3
> --- requires (length (elemIndices Trade (headSchedule s)) == 1)
>     where ins0 = ins
>           s0   = s
>           ins1 = filter isAccept ins0
>           ins2 = filter isOffer ins0
>           s1   = process s0 ins1
>           s2   = process s1 ins2
>           s3   = if (ins1 == [])
>                  then s3'
>                  else s3''
>           s3'  = if (unsafeRandomize (makeUniformSP [True, False]))
>                  then prepareOffer s2
>                  else s2
>           s3'' = unschedule [Trade] s2


> communicateAction :: [Msg] -> State -> State
> communicateAction ins s 
> --- requires (headSchedule s == [Communicate])
>     = process s1 ins2
> 	    where s0 = s 
>	    	  ins0 = ins 
>                 s1 = unschedule [Communicate] s0
>	    	  ins1 = filter (not . isOffer) ins
>		  ins2 = filter (not . isAccept) ins1

                     
> learnAction :: [Msg] -> State -> State
> learnAction ins s = s2
>     where s0  = s
>     	    s1  = foldl f s0 ins
>	    f s (Offer gqss, k) = appendMsg (Accept [], k) s 
>           f s (Observe, k) = processObserve k s
>           f s (Reveal u gps, k) = processReveal u gps s
>           f s (_, _) = s 
>           s2  = if (others /= [])
>                 then appendMsg msg s1
>                 else unschedule [Learn] s1
>           others = otherOwnKindIds s1
>           msg = (Observe, k)
>           k   = unsafeRandomize sp
>           sp  = makeUniformSP others


> mutateAction :: [Msg] -> State -> State
> mutateAction ins s= unschedule [Mutate] (setGoodPrices gps1 s)
>	where gps = goodPrices s
>	      gps1 = unsafeRandomizePrices mr mpd gps
>             mr = mutRate s
>             mpd = mutPricesDist s
                               

> process :: State -> [Msg] -> State
> process = foldl f
>     where f s (Offer gqss, k) = processOffer gqss k s
>           f s (Accept gqs, k) = processAccept gqs s
>           f s (Observe, k) = processObserve k s
>           -- f s (Reveal u gps, k) = processReveal u gps s
>           f s (_, _) = s 

> processObserve :: AgentId -> State -> State
> processObserve id s = if (elem Learn (headSchedule s) )
> 		      	then appendMsg (Reveal (utility s) (goodPrices s), id ) s
>			else s




> processReveal :: Utility ->
>                  [(Good, Price)] -> 
>                  State -> 
>                  State  
> processReveal u gps s = s2
>     where s0 = s
>           s1 = if (u < utility s0) 
>                then s0
>                else setGoodPrices gps s0
>           s2 = unschedule [Learn] s1

-- Choose according to selectTrade the trade to accept 
-- given the proposed ones ogqs
-- Consequently update the stock of goods, possibly consume,
-- and answers the proposer of the trade  
	 
> processOffer :: [Trade]-> AgentId ->  State -> State 
> processOffer ogqss k s = s2
>     where s0 = s
>     	    agqs = selectTrade ogqss s0
>	    s1 = appendMsg (Accept agqs,k) s
>     	    s2 = appendGoodQuantities (rescale agqs (-1.0)) s1
	

-- first checks if there is a proposal to trade against the good produced. 
-- accepts it if the value of the trade is positive and scales it down
-- according to availabilitythe value of the stock.

> selectTrade :: [Trade] -> State -> Trade
> selectTrade ogqss s = agqs2 
>     where  agqs2 = if (agqss1 == [])
>	    	     then [] 
>		     else rescale agqs1 ratio1
>	     ratio1 = minimum [1.0, (eval gqs g) / (eval agqs1 g)]
>	     agqs1 = normalize (head (agqss1)) 
>            agqss1 = filter (isTradeWorth) agqss0
>	     agqss0  = filter (isTradeProposed) ogqss
>	     isTradeProposed xgqs = ( (eval xgqs g) > 0)  
>	     isTradeWorth xgqs = ( (dot gps xgqs) <= 0)
>            gps = goodPrices s
>            gqs = goodQuantities s
>	     g = goodProduced s 		    



> processAccept :: Trade -> State -> State 
> processAccept agqs s = s1
> 	where s0 = s
>	      s1 = appendGoodQuantities agqs s
>	      -- s2 = appendMsg (Done, (masterId s1)) s1


> isConsume :: State -> Bool
> isConsume s = areAllPositive goods
>     where goods = [eval gqs g | (g, q) <- sgqs]
>           gqs = goodQuantities s
>           sgqs = utilityWeights s

--> isConsume s = (sort (map fst (goodQuantities s)) == goods)

> prepareOffer :: State -> State
> prepareOffer s = s2
> 	where s0  = s
>	      sp1 = makeUniformSP (scarfConsumerIds s0)
>             k   = unsafeRandomize sp1    
>             msg = (Offer (offers s0), k)
>             s1  = appendMsg msg s0 
>             s2  = pushSchedule [Communicate] s1

> offers :: State -> [Trade]
> offers s = case (kind s) of
>            ScarfConsumer g sgqs 
>	     	        -> maxScarfUtilityOffers g sgqs gps gqs
>	     SomeGoodsScarfConsumer g sgqs
>	     		-> scarfSomeGoodsOffers g sgqs gps gqs
>     where gps = goodPrices s
>           gqs = goodQuantities s     

-->            DaltonScarfConsumer g sgqs -> constOffers g gps gqs

	 		  	      
