> module RandomUtil where

> import List
> import Maybe
> import Random
> import Numeric
> import SimpleProb
> import EnvWithMaster
> import IOExts

> randomize :: SimpleProb a -> IO a
> randomize (SP ds) = do x <- randomRIO (0.0, 1.0)
>                        xs <- return (map sum (tail (inits (map snd ds))))
>                        k <- return  (fromJust (findIndex (> x) xs))
>                        return (fst (ds!!k))

> toUniformSP :: [a] -> SimpleProb a
> toUniformSP xs = SP [(x, p) | x <- xs]
>     where p = 1.0 / (realToFrac (length xs))

> convexCombination :: Float -> SimpleProb a -> SimpleProb a -> SimpleProb a
> convexCombination t (SP ds1)  (SP ds2) = SP ds3
>      where ds3= [(x,t*p)| (x,p) <- ds1 ] ++ [(x,(1-t)*p)| (x,p) <- ds2]    

> sp1' :: SimpleProb Int
> sp1'  = SP [(-1, 0.5), (1, 0.5)]

> sp2' :: SimpleProb Int
> sp2'  = SP [(1, 0.5), (-1, 0.25), (2, 0.125), (-1, 0.125)]



> randomizePrices :: [(Good,Price)]-> [(Good,Price)]
> randomizePrices gps =gps1
>  where gps0 =gps
>	 sp= toUniformSP [1.0 .. 100.0]
>	 toMutSP p = convexCombination mutRate sp (SP [(p,1)])
> 	 gps1= [ (g,unsafePerformIO (randomize (toMutSP p) )) | (g,p) <- gps0 ]


> shuffleList :: Eq a => [a] -> IO [a] 
> shuffleList xs = if null xs 
>                     then return xs
>                     else
>                         do 
>       	      	    x  <- randomize (toUniformSP xs)
>                           xs' <- shuffleList (delete x xs)
>                           return (x:xs')


