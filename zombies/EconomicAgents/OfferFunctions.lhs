> module EconomicAgents.OfferFunctions where

> import List

> import Util.List.Ops
> import Env
> import EconomicAgents.ScarfOps


-- > constOffers :: Good -> 
-- >                [(Good, Price)] -> 
-- >                [(Good, Quantity)] -> 
-- >                [Trade] 
-- > constOffers g gps gqs = [exchangeDemand gq j gps | j<- goods  ]
-- >  where gq = (g, eval gqs g)



> scarfSomeGoodsOffers :: Good -> 
>                         [(Good, Quantity)] -> 
>                         [(Good, Price)] -> 
>                         [(Good, Quantity)] -> 
>                         [Trade]
> scarfSomeGoodsOffers g sgqs gps gqs = 
>     [exchangeOffer (tGood,eval gqs tGood) cGood gps,
>      exchangeOffer (tGood,eval gqs tGood) pGood gps,	      
>      exchangeOffer (pGood,eval gqs pGood) cGood gps,	
>      exchangeOffer (pGood,0.5 *(eval gqs pGood)) tGood gps,	
>      exchangeOffer (cGood,eval gqs cGood) pGood gps,	
>      exchangeOffer (cGood, eval gqs cGood) tGood gps]	
>  where pGood = g 
> 	 cGood = ((g + 1) `rem` nbGoods)   
>	 tGood = ((g + 2) `rem` nbGoods )
>	 nbGoods = length goods
>        goods = nub (map fst sgqs)


--> Offer gq against the corresponding quantity of good g� according to gps


> exchangeOffer :: (Good, Quantity) -> Good -> [(Good,Price)] -> Trade
> exchangeOffer gq g' gps = [gq',(g,-1.0*q)]
>  	   where gq' = (g' , (p / p')*q)
>	   	 g = fst gq
>	   	 q =snd gq 
>	   	 p = eval gps g
>		 p' = eval gps g'





> maxScarfUtilityOffers :: Good -> 
>                          [(Good, Quantity)] -> 
>                          [(Good, Price)] -> 
>                          [(Good, Quantity)] -> 
>                          [Trade]
> maxScarfUtilityOffers g sgqs gps gqs = [exchangeDemand gq g gps | gq <- dgqs]
>  where ogqs = scarfOptimum sgqs gqs gps 
> 	 dgqs = normalize ( (rescale gqs (-1.0)) ++ ogqs)





--> Demand gq against the corresponding quantity of good g according to gps

> exchangeDemand :: (Good, Quantity) -> Good -> [(Good,Price)] -> Trade
> exchangeDemand gq g gps = [gq,gq']
>  	   where gq' = (g , (p / p') * q * (-1.0) )
>	   	 q = snd gq 
>	   	 p = eval gps (fst gq)
>		 p' = eval gps g




