> module EconomicAgents.Action where


> data Action = Idle
>             | Produce 
>             | Consume
>             | Trade 
>             | Learn 
>	      | Communicate
>	      | Mutate
>	      | Obey
>             | LaunchProduce
>             | LaunchCopy 
>             | DirectCopy  
>             | LaunchTrade 
>             | DirectTrade  
>             | LaunchMutate 
>	      | LaunchConsume
>               deriving (Eq, Show)


