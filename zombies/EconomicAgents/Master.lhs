> module EconomicAgents.Master(makeMaster) where

> import List

> import NumericTypes.Nat
> import NumericTypes.Real
> import Agent.Agent
> import Prob.SimpleProb
> import Prob.SimpleProbOps
> import Util.Ops
> import Util.Random.Ops
> import Util.Unsafe.Random.Ops
> import Util.List.Ops

> import EconomicAgents.Action
> import EconomicAgents.Schedule

> import Env



Global parameters

> repRate :: REAL
> repRate = 0.1

> maxtries :: Int
> maxtries = 1


The Master's internal state

> type State = (AgentId, 
>               [(AgentId, AgentKind)], 
>               Schedule, 
>               [Msg])

has to fulfill the following invariant:

s = (i, iks, sch, outs)
=>
(msg, i) elem outs => i elem (map fst iks)



Constructors

> makeMaster :: AgentId -> 
>               [(AgentId, AgentKind)] -> 
>               Agent RR
> makeMaster i iks = Agent ident
>                          outMsgs
>                          step
>                          showMaster
>                          (i,iks, sch, [])
>     where sch = makeSchedule [(LaunchProduce,1),(LaunchTrade,1),
>			        (LaunchCopy,10),(LaunchCopy,10)]


Auxiliary functions (core)

> ident :: State -> AgentId
> ident (i, iks, sch, outs) = i

> outMsgs :: State -> [Msg] 
> outMsgs (i, iks, sch, outs) = outs

> step :: State -> [Msg] -> State
> step s ins = foldr f s'' actions
>     where (actions, s') = popSchedule s
>           s'' = emptyMsgs s'
>	    f LaunchProduce = launchProduceAction ins
>           f LaunchConsume = launchConsumeAction ins 
>           f LaunchCopy = launchCopyAction ins
>	    f DirectCopy = directCopyAction ins
>           f LaunchTrade = launchTradeAction ins
>	    f DirectTrade = directTradeAction ins  
>           f LaunchMutate = launchMutateAction ins  
>           f Idle = idleAction ins

-- > printMaster :: State -> IO () 
-- > printMaster (i, iks, sch, outs) 
-- >     = do print ("id:   " ++ (show i))
-- >          print ("iks:  " ++ (show iks))
-- >          print ("sch:  " ++ (show sch))
-- >          print ("outs: " ++ (show outs))

> showMaster :: State -> String -> String

> showMaster (i, iks, sch, outs) ""
>     = "id:   " ++ (show i) ++ ", " ++
>       "iks:  " ++ (show iks) ++ ", " ++
>       "sch:  " ++ (show sch) ++ ", " ++
>       "outs: " ++ (show outs)


Auxiliary functions (helpers)

Features

> idKinds :: State -> [(AgentId, AgentKind)]
> idKinds (i, iks, sch, outs) = iks

> ids :: State -> [AgentId]
> ids s = map fst (idKinds s)	

> kind :: State -> AgentId -> AgentKind 
> kind (i, iks, sch, outs) j = k
>       where [(h,k)] = filter (test) iks
> 	      test (a,b) = (a == j)

> kinds :: State -> [AgentKind]
> kinds s = nub (map snd (idKinds s))

> schedule :: State -> Schedule
> schedule (i, iks, sch, outs) = sch


Commands

> emptyMsgs :: State -> State
> emptyMsgs (i, iks, sch, outs) = (i,iks, sch , [])

> appendMsgs :: [Msg]-> State -> State
> appendMsgs msgs (i, iks, sch, outs) = (i,iks, sch , outs++msgs)

> testkind :: AgentKind -> State -> AgentId -> Bool
> testkind k s j = (kind s j == k)   

> popSchedule :: State ->  ([Action], State)
> popSchedule (i, iks, sch, outs) =
>     (head sch, (i, iks, tail sch, outs))

> pushSchedule :: [Action] -> State -> State
> pushSchedule actions (i, iks, sch, outs) =
>	(i, iks, actions:sch, outs)


Actions

> idleAction :: [Msg] -> State -> State
> idleAction ins s = pushSchedule [Idle] s

> launchMutateAction :: [Msg] -> State -> State
> launchMutateAction ins  s = appendMsgs  [(MustMutate, k) | k <- (ids s)] s

> launchProduceAction :: [Msg] -> State -> State
> launchProduceAction ins s = appendMsgs  [(MustProduce, k) | k <- (ids s)] s

> launchConsumeAction :: [Msg] -> State -> State
> launchConsumeAction ins s = appendMsgs  [(MustConsume, k) | k <- (ids s)] s



-- Builds the list xs :: [(AgentId,Good)] of trades to be launched.
--  An element (i,g) in xs stands for agent i trying to buy good g.  
-- To be corrected : an agent should not try to buy the good he produces 

> launchTradeAction ::  [Msg] ->State -> State
> launchTradeAction ins s = s2
>     where s1 = pushSchedule [DirectTrade]  s
>           s2 = appendMsgs [(StillToTrade xs,ident s1),        
>	       	 	     (MustDemand (snd (head xs)),fst(head xs)) ] s1
>           xs = [(i,g) | i <- ags , g <- goods , n <- [1..maxtries ]]
>           ags = concat agss1 
>           agss1 = map unsafeRandomPermute agss0  
>           agss0 = [agsof k | k <- orderkinds]
>           agsof k = filter (testkind k s) (ids s)
>           orderkinds = unsafeRandomPermute (kinds s)
	    
--  first checks if the list of trade to be performed is non-empty.
-- If this is the case, check if the current trade is Done
-- If this is the case  discard the first element of xs and launch 
-- a new trade    
-- Otherwise keep the same list of trades. 


> directTradeAction :: [Msg] -> State -> State
> directTradeAction ins s = s3
>     where s3 =  if  xs ==[] 
>                 then s0 
>                 else s2
>           s2 = pushSchedule [DirectTrade] s1
>           s1 = if (foldr isTradeDone False ins) 
>                then appendMsgs [(StillToTrade (tail(xs)), ident s0),
>                                 (MustDemand (snd (head xs)),fst(head xs))] s0
>                else  appendMsgs [(StillToTrade xs, ident s0)] s0	
>           s0 = s
>           xs = foldr extractList [] ins
>           extractList (StillToTrade ys, _) xs = xs ++ ys
>           extractList (_, _) xs = xs
>           isTradeDone (Done,_) _  = True 
>           isTradeDone (_,_) x = x

> launchCopyAction ::  [Msg] ->State -> State
> launchCopyAction ins s = s2
>     where s1 = appendMsgs  [(MustRevealUtility, k) | k <- ids s] s
>           s2 = pushSchedule	[DirectCopy] s1
			
> directCopyAction :: [Msg] -> State -> State 
> directCopyAction ins s = if areAllReveal then s' else s''
>     where areAllReveal= ( sort (map fst ags0) == sort (ids s))
>           ags0 = foldr f [] ins
>           f  (Reveal u gps ,k) xs = xs++[(k,u)]
>           f (_,_) xs = xs
>           s''= pushSchedule [DirectCopy] s 
>           s' = foldr assignCopy s sortedByKinds
>           sortedByKinds = [agsof k | k <- (kinds s)]
>           agsof k = filter ((testkind k s). fst) ags1
>           ags1 = map (cross (id, (score us))) ags0
>           us = map (snd) ags0

rags is the list of agents that must copy.
cags is the list of agents to be copied
It must be that
 x in rags if and only if x not in cags

> assignCopy ::  [(AgentId,REAL)] -> State -> State    
> assignCopy ags s = appendMsgs [(MustCopy i ,k) | (k,i) <- pairs] s
>     where pairs = map (selectModel cags) rags
>           (rags,cags) = formGroups ags

-- > formGroups :: [(AgentId, REAL)] -> ([(AgentId, REAL)],[(AgentId, REAL)]) 
-- > formGroups ags = (rags, cags) 
-- >     where (rags,cags) = until (rateReach repRate) f ([],ags)
-- >           f (rs,cs) = if (isReplaced (head(cs)) ) 
-- >                       then (rs ++ [head(cs)],tail(cs))
-- >                       else (rs,tail(cs) ++ [head(cs)]) 
-- >           isReplaced (id,x) = unsafePerformIO (randomize ( 
-- >		    	       	      		 SP [(True,1-x),
-- > (False,x)]))

> formGroups :: [(AgentId, REAL)] -> ([(AgentId, REAL)], [(AgentId, REAL)]) 
> formGroups ags = until pred f ([], ags)
>     where pred (iss, iss') = fromIntegral (length iss) > n
>           n = (fromIntegral (length ags)) * repRate
>           f (iss, iss') = if (unsafeRandomize (sp is') == True) 
>                           then ((is':iss), iss')
>                           else (iss, iss') 
>               where is' = unsafeRandomize (makeUniformSP iss')
>                     sp (i, s) = SP [(True, 1.0 - s), (False, s)]

> selectModel :: [(AgentId,REAL)] -> (AgentId, REAL) -> (AgentId, AgentId)
> selectModel iss is = (i, i') 
>     where i = fst is
>           i' = fst is'
>           is' = iss!!k
>           (k, b) = until pred f (0, False)
>           pred (j, b) = (b == True)
>           f (j, b) = if (unsafeRandomize (sp is') == True) 
>                      then (j', True)
>                      else (j', False) 
>               where is' = iss!!j'
>                     j' = unsafeRandomize (makeUniformSP [0..n])
>                     n = length iss - 1
>                     sp (i, s) = SP [(True, s), (False, 1.0 - s)]

-- > selectModel :: [(AgentId,REAL)] -> (AgentId, REAL) -> (AgentId, AgentId)
-- > selectModel ags (i, s) = (i, j) 
-- >     where j = fst ( head(bgs) )
-- >           (bgs, cgs) = until pred f ([], ags)
-- >           pred 
-- >           f (rs,cs)= if (isCopied (head(cs)) ) 
-- >                      then (rs++[head(cs)], tail(cs))
-- >                      else (rs,tail(cs) ++ [head(cs)]) 
-- >           isCopied (id,x) = unsafePerformIO (randomize (SP
-- >			  	   	        [(True,x), (False,1-x)])) 

> score :: [REAL] -> REAL -> REAL
> score us u = x 
>     where umax = maximum us
>           umin = minimum us
>           x = if (umax > umin)
>               then (u - umin ) / (umax - umin)  
>               else 0.5

























