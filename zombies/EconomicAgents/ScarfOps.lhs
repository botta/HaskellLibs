> module EconomicAgents.ScarfOps where

> import List

> import Util.List.Ops
> import NumericTypes.Real

> import Env

scarfUtility

requires:

- normalize sgqs == sgqs
- (g,q) elem sgqs => q > 0

> scarfUtility :: (Eq a) => [(a, REAL)] -> [(a, REAL)] -> REAL
> scarfUtility sgqs gqs = r
>     where r = minimum [(eval gqs g) / (eval sgqs g) | g <- gs]
>           gs = map fst sgqs

ensures:

- setify (map fst sgqs) contains setify (map fst gqs)
- scafUtility gqs gqs == 1


scarfOptimum

> scarfOptimum :: (Eq a) => 
>                 [(a, REAL)] -> 
>                 [(a, REAL)] -> 
>                 [(a, REAL)] -> 
>                 [(a, REAL)]

> scarfOptimum sgqs gqs gps = rescale sgqs ratio
>     where ratio = ((dot gps gqs) / (dot gps sgqs))

ensures: 

- dot gps ogqs == dot gps gqs 
  where ogqs = scarfOptimum sgqs gqs gps




Possibly implement this function in ScarfConsumer.consumeAction locally !

> scarfSomeGoodsConsumption :: Good -> [(Good,Quantity)]->[(Good,Quantity)]
>  		      	            -> [(Good,Quantity)]
> scarfSomeGoodsConsumption g sgqs gqs = rescale sgqs' consRate
>      where sgqs' = filter (isConsumed.fst) sgqs 
>      	     consRate = if  ( (minimum qs) > 0) 
> 	      	        then  minimum ratios
>		        else 0
>	     qs = [eval gqs g | g <- goodsConsumed]		 
> 	     ratios = [(eval gqs g) / (eval sgqs' g) | g <- goodsConsumed]
>	     goodsConsumed = filter isConsumed goods
>	     isConsumed g' = not (g'== tGood)
>	     tGood = (g+2) `rem` nbGoods  
>	     nbGoods = length goods
>            goods = nub (map fst sgqs)



scarfConsumption

> scarfConsumption :: [(Good,Quantity)]->[(Good,Quantity)]-> [(Good,Quantity)]
> scarfConsumption sgqs gqs = rescale sgqs consRate 
> 		where consRate = if  ( minimum qs >0) 
>		      	       	 then  minimum ratios
>				 else 0
>		      qs = [eval gqs g | g <- goods]		 
>		      ratios = [eval gqs g / eval sgqs g | g <- goods]
>                     goods = nub (map fst sgqs)
 
