> module Ord.Ord where

> maxLE :: Ord a => [a] -> a -> Maybe a
> maxLE [] _ = Nothing
> maxLE (x : xs) y = case (maxLE xs y) of  
>   Nothing -> if x <= y then Just x else Nothing
>   Just z  -> if x <= y && z <= x then Just x else Just z

> minGE :: Ord a => [a] -> a -> Maybe a
> minGE [] _ = Nothing
> minGE (x : xs) y = case (minGE xs y) of  
>   Nothing -> if x >= y then Just x else Nothing
>   Just z  -> if x >= y && z >= x then Just x else Just z

